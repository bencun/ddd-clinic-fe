import { useEffect } from 'react';
import { Route, Routes } from 'react-router';
import { AuthLayout } from './components/layouts/AuthLayout';
import { MainLayout } from './components/layouts/MainLayout';
import { ToastController } from './components/organisms/ToastController';
import { ClinicPage } from './components/pages/clinic/ClinicPage';
import { ExaminationsPage } from './components/pages/examinations/ExaminationsPage';
import { LoginPage } from './components/pages/auth/LoginPage';
import { PatientsPage } from './components/pages/patients/PatientsPage';
import { SchedulePage } from './components/pages/schedule/SchedulePage';
import { UsersPage } from './components/pages/users/UsersPage';
import { AuthGuard } from './components/utils/AuthGuard';
import { useAuthStore } from './framework/store/AuthStore';
import { Role } from './framework/types/Role';
import { ViewPatientPage } from './components/pages/patients/ViewPatientPage';
import { NewPatientPage } from './components/pages/patients/NewPatientPage';
import { ViewUserPage } from './components/pages/users/ViewUserPage';
import { CreateUserPage } from './components/pages/users/CreateUserPage';
import { CreatePatientPage } from './components/pages/patients/CreatePatientPage';
import { setupSockets } from './framework/hooks/setupSockets';
import { ExaminationViewPage } from './components/pages/examinations/ExaminationViewPage';

function App() {
  const { initialize, initialized } = useAuthStore(
    ({ initialize, initialized }) => ({
      initialize,
      initialized,
    })
  );

  // Auth init
  useEffect(() => {
    initialize();
  }, []);

  setupSockets();

  // Loading while initializing
  if (!initialized) {
    return null;
  }

  // Routing tree
  return (
    <div>
      <Routes>
        {/* App routes */}
        <Route
          element={
            <AuthGuard redirectToLogin>
              <MainLayout />
            </AuthGuard>
          }
        >
          <Route path="/" element={'Home.'} />
          <Route path="/users">
            <Route
              index
              element={
                <AuthGuard roles={[Role.ADMIN]}>
                  <UsersPage />
                </AuthGuard>
              }
            ></Route>
            <Route
              path=":uuid"
              element={
                <AuthGuard roles={[Role.ADMIN]}>
                  <ViewUserPage />
                </AuthGuard>
              }
            ></Route>
            <Route
              path="create"
              element={
                <AuthGuard roles={[Role.ADMIN]}>
                  <CreateUserPage />
                </AuthGuard>
              }
            ></Route>
          </Route>
          <Route
            path="/clinic"
            element={
              <AuthGuard roles={[Role.ADMIN]}>
                <ClinicPage />
              </AuthGuard>
            }
          />
          <Route path="/patients">
            <Route
              index
              element={
                <AuthGuard roles={[Role.PRACTITIONER, Role.NURSE]}>
                  <PatientsPage />
                </AuthGuard>
              }
            />
            <Route
              path="create"
              element={
                <AuthGuard roles={[Role.NURSE]}>
                  <CreatePatientPage />
                </AuthGuard>
              }
            />
            <Route path=":uuid">
              <Route
                index
                element={
                  <AuthGuard roles={[Role.PRACTITIONER, Role.NURSE]}>
                    <ViewPatientPage />
                  </AuthGuard>
                }
              />
              <Route
                path="records"
                element={
                  <AuthGuard roles={[Role.PRACTITIONER, Role.NURSE]}>
                    <ViewPatientPage showHealthRecords />
                  </AuthGuard>
                }
              ></Route>
            </Route>
          </Route>
          <Route
            path="/patients/new"
            element={
              <AuthGuard roles={[Role.NURSE]}>
                <NewPatientPage />
              </AuthGuard>
            }
          />
          <Route
            path="/schedule/:date"
            element={
              <AuthGuard roles={[Role.NURSE, Role.PRACTITIONER]}>
                <SchedulePage />
              </AuthGuard>
            }
          />
          <Route
            path="/schedule"
            element={
              <AuthGuard roles={[Role.NURSE, Role.PRACTITIONER]}>
                <SchedulePage />
              </AuthGuard>
            }
          />
          <Route path="/examinations">
            <Route
              index
              element={
                <AuthGuard roles={[Role.PRACTITIONER]}>
                  <ExaminationsPage />
                </AuthGuard>
              }
            ></Route>

            <Route
              path=":uuid"
              element={
                <AuthGuard roles={[Role.NURSE]}>
                  <ExaminationViewPage />
                </AuthGuard>
              }
            ></Route>
          </Route>
        </Route>

        {/* Auth routes */}
        <Route element={<AuthLayout />}>
          <Route path="/login" element={<LoginPage />} />
          <Route path="/register" element={<div>Register route</div>} />
        </Route>
      </Routes>
      <ToastController />
      <div id="modal-portal"></div>
    </div>
  );
}

export default App;

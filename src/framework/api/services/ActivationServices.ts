import { User } from '../../models/User';
import { VoidType } from '../../types/VoidType';
import { APIService } from '../../utils/APIService';
import { ActivationDTO } from './dtos/ActivationDTO';
import { EmailDTO } from './dtos/EmailDTO';
import { PasswordResetOrForgotVerifyDTO } from './dtos/PasswordResetOrForgotVerifyDTO';

export const AdminRequestPasswordResetService = async (dto: EmailDTO) =>
  await new APIService('/activation/resetPasswordStart', VoidType).post(
    dto,
    VoidType
  );

export const AdminChangeEmailService = async (uuid: string, dto: EmailDTO) =>
  await new APIService(`/activation/${uuid}/changeEmail`, VoidType).post(
    dto,
    VoidType
  );

export const GetUserActivationDataService = async (token: string) =>
  await new APIService(`/activation/token/${token}`, User).get({}, User);

export const PostUserActivationDataService = async (
  token: string,
  dto: ActivationDTO
) => await new APIService(`/activation/token/${token}`, User).post(dto, User);

export const UserForgotPasswordService = async (dto: EmailDTO) =>
  await new APIService('/activation/forgotPasswordStart', VoidType).post(
    dto,
    VoidType
  );

export const VerifyResetOrForgottenPasswordService = async (
  dto: PasswordResetOrForgotVerifyDTO
) =>
  await new APIService(
    '/activation/resetOrForgotPasswordVerify',
    VoidType
  ).post(dto, VoidType);

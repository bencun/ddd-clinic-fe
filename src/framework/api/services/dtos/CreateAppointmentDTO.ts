export interface CreateAppointmentDTO {
  patientUuid: string;
  patientName: string;
  start: string;
  duration: number;
}

export interface ActivationDTO {
  password: string;
  passwordConfirm: string;
}

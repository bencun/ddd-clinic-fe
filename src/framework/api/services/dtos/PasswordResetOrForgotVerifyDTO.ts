import { ActivationDTO } from './ActivationDTO';

export interface PasswordResetOrForgotVerifyDTO extends ActivationDTO {
  token: string;
}

import { Role } from '../../../types/Role';

export interface CreateUserDTO {
  name: string;
  email: string;
  phone: string;
  role: Role;
}

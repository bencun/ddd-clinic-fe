export interface UpdateUserDTO {
  name: string;
  email: string;
  phone: string;
}

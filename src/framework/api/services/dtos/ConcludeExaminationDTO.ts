import { PrescriptionDTO } from '../../../models/HealthRecord';

export interface ConcludeExaminationDTO {
  completionType: 'cancel' | 'complete';
  anamnesis: string;
  prescriptions: PrescriptionDTO[];
  diagnoses: string[];
  chronicDiagnosesEstablished: string[];
  chronicDiagnosesRemoved: string[];
  notes: string;
}

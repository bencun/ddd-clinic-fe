export interface RescheduleAppointmentDTO {
  start?: string;
  duration?: number;
}

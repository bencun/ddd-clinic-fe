export interface UpdatePreferencesDTO {
  preferences: {
    key: string;
    value: string | number | null | boolean;
  }[];
}

import { APIService } from '../../utils/APIService';
import { format } from 'date-fns';
import { Schedule } from '../../models/Schedule';
import { CreateAppointmentDTO } from './dtos/CreateAppointmentDTO';
import { Uuid } from '../../types/Uuid';
import { VoidType } from '../../types/VoidType';
import { RescheduleAppointmentDTO } from './dtos/RescheduleAppointmentDTO';

export const GetAllSchedulesService = async (date: Date) => {
  const formattedDate = format(date, 'yyyy-MM-dd');
  return await new APIService('/schedule', Schedule).getOne(formattedDate);
};

export const CreateAppointmentService = async (
  appointment: CreateAppointmentDTO
) => {
  return await new APIService('/schedule', VoidType).post(appointment, Uuid);
};

export const StartAppointmentService = async (uuid: string) => {
  return await new APIService('/schedule/startAppointment', VoidType).patch(
    uuid,
    VoidType
  );
};

export const CancelAppointmentService = async (uuid: string) => {
  return await new APIService('/schedule/cancelAppointment', VoidType).patch(
    uuid,
    VoidType
  );
};

export const RescheduleAppointmentService = async (
  uuid: string,
  dto: RescheduleAppointmentDTO
) => {
  return await new APIService(
    '/schedule/rescheduleAppointment',
    VoidType
  ).patch(uuid, VoidType, dto);
};

import { LOGIN_URL, REFRESH_URL, LOGOUT_URL } from '../../constants/AuthURLs';
import { User } from '../../models/User';
import { VoidType } from '../../types/VoidType';
import { APIService } from '../../utils/APIService';

export const AuthLoginService = new APIService(LOGIN_URL, User);
export const AuthRefreshService = new APIService(REFRESH_URL, User);
export const AuthLogoutService = new APIService(LOGOUT_URL, VoidType);

import { Patient } from '../../models/Patient';
import { APIService } from '../../utils/APIService';

export const PatientAPIService = new APIService('/patients', Patient);

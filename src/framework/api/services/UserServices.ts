import { APIService } from '../../utils/APIService';
import { User } from '../../models/User';
import { CreateUserDTO } from './dtos/CreateUserDTO';
import { UpdateUserDTO } from './dtos/UpdateUserDTO';
import { VoidType } from '../../types/VoidType';

export const GetUserService = async (uuid: string) =>
  await new APIService('/users', User).getOne(uuid);

export const GetAllUsersService = async () =>
  await new APIService('/users', User).getAll({});

export const CreateUserService = async (dto: CreateUserDTO) =>
  await new APIService('/users', User).create(dto, User);

export const UpdateUserService = async (uuid: string, dto: UpdateUserDTO) =>
  await new APIService('/users', User).update(uuid, dto, User);

export const DeleteUserService = async (uuid: string) =>
  await new APIService('/users', User).delete(uuid, VoidType);

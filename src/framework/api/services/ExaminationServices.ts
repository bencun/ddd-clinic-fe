import { Examination } from '../../models/Examination';
import { VoidType } from '../../types/VoidType';
import { APIService } from '../../utils/APIService';
import { ConcludeExaminationDTO } from './dtos/ConcludeExaminationDTO';

export const GetActiveExaminationsService = async () =>
  await new APIService('/examinations/active', Examination).getAll({});

export const GetExaminationService = async (uuid: string) =>
  await new APIService('/examinations', Examination).getOne(uuid);

export const ConcludeExaminationService = async (
  uuid: string,
  params: ConcludeExaminationDTO
) =>
  await new APIService(`/examinations/conclude/${uuid}`, VoidType).post(
    params,
    VoidType
  );

import { Clinic } from '../../models/Clinic';
import { APIService } from '../../utils/APIService';
import { UpdatePreferencesDTO } from './dtos/UpdatePreferencesDTO';

export const GetClinicPreferencesService = async () =>
  await new APIService('/clinic/getPreferences', Clinic).get({}, Clinic);

export const SetClinicPreferencesService = async (
  prefs: UpdatePreferencesDTO
) => await new APIService('/clinic/setPreferences', Clinic).post(prefs, Clinic);

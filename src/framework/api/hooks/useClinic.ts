import { useState } from 'react';
import { TClinicForm } from '../../../components/molecules/ClinicForm';
import { useLoading } from '../../hooks/useLoading';
import { Clinic, ClinicPreferenceKeys } from '../../models/Clinic';
import { useToastStore } from '../../store/ToastStore';
import { ExceptionDetails } from '../../utils/ExceptionDetails';
import {
  GetClinicPreferencesService,
  SetClinicPreferencesService,
} from '../services/ClinicServices';

export function useClinic() {
  const { isLoading, startLoading, stopLoading } = useLoading();
  const [clinic, setClinic] = useState<Clinic>();
  const toast = useToastStore();

  const load = async () => {
    setClinic(undefined);
    startLoading();
    try {
      const res = await GetClinicPreferencesService();
      if (res.success && res.data) {
        setClinic(res.data);
      } else throw new Error(res.error);
    } catch (e) {
      toast.show({
        text: 'Failed to load the clinic preferences.',
        type: 'error',
      });
    } finally {
      stopLoading();
    }
  };

  const save = async (dto: TClinicForm) => {
    setClinic(undefined);
    startLoading();
    try {
      const res = await SetClinicPreferencesService({
        preferences: Object.keys(dto).map((key) => ({
          key,
          value: dto[key as ClinicPreferenceKeys],
        })),
      });
      if (res.success && res.data) {
        setClinic(res.data);
      } else throw new Error(res.error);
    } catch (e) {
      toast.show({
        text: ExceptionDetails('Failed to save the clinic preferences.', e),
        type: 'error',
      });
    } finally {
      stopLoading();
    }
  };

  return { isLoading, clinic, load, save };
}

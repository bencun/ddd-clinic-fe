import { useEffect, useState } from 'react';
import { useLoading } from '../../hooks/useLoading';
import { Examination } from '../../models/Examination';
import { useToastStore } from '../../store/ToastStore';
import { ExceptionDetails } from '../../utils/ExceptionDetails';
import { ConcludeExaminationDTO } from '../services/dtos/ConcludeExaminationDTO';
import {
  ConcludeExaminationService,
  GetActiveExaminationsService,
  GetExaminationService,
} from '../services/ExaminationServices';

export function useExamination(uuid?: string) {
  const [examination, setExamination] = useState<Examination>();
  const [examinations, setExaminations] = useState<Examination[]>();
  const { isLoading, startLoading, stopLoading } = useLoading();
  const toast = useToastStore();

  const load = async (_uuid: string) => {
    setExamination(undefined);
    startLoading();
    try {
      const res = await GetExaminationService(_uuid);
      if (res.success && res.data) {
        setExamination(res.data);
      } else throw new Error(res.error);
    } catch (e) {
      toast.show({
        text: ExceptionDetails('Examination failed to load', e),
        type: 'error',
      });
    } finally {
      stopLoading();
    }
  };

  const loadActive = async () => {
    startLoading();
    try {
      const res = await GetActiveExaminationsService();
      if (res.success && res.data) {
        setExaminations(res.data);
      } else throw new Error(res.error);
    } catch (e) {
      toast.show({
        text: ExceptionDetails('Failed to load active examinations.', e),
        type: 'error',
      });
    } finally {
      stopLoading();
    }
  };

  const conclude = async (_uuid: string, dto: ConcludeExaminationDTO) => {
    startLoading();
    try {
      const res = await ConcludeExaminationService(_uuid, dto);
      if (res.success) {
        if (dto.completionType === 'cancel') {
          toast.show({ text: 'Examination cancelled.', type: 'success' });
        }
        if (dto.completionType === 'complete') {
          toast.show({ text: 'Examination completed.', type: 'success' });
        }
      } else throw new Error(res.error);
    } catch (e) {
      toast.show({
        text: ExceptionDetails('Failed to conclude the examination.', e),
        type: 'error',
      });
    } finally {
      stopLoading();
    }
  };

  useEffect(() => {
    if (uuid) load(uuid);
  }, []);

  return { examination, examinations, load, loadActive, conclude, isLoading };
}

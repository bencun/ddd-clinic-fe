import { useEffect, useState } from 'react';
import { TAppointmentForm } from '../../../components/molecules/AppointmentForm';
import { useLoading } from '../../hooks/useLoading';
import { Schedule } from '../../models/Schedule';
import { useToastStore } from '../../store/ToastStore';
import { ExceptionDetails } from '../../utils/ExceptionDetails';
import { RescheduleAppointmentDTO } from '../services/dtos/RescheduleAppointmentDTO';
import {
  CancelAppointmentService,
  CreateAppointmentService,
  GetAllSchedulesService,
  RescheduleAppointmentService,
  StartAppointmentService,
} from '../services/ScheduleServices';

export function useSchedule(date?: Date) {
  const [schedule, setSchedule] = useState<Schedule>();
  const { isLoading, startLoading, stopLoading } = useLoading();
  const toast = useToastStore();

  const load = async (_date: Date) => {
    setSchedule(undefined);
    startLoading();
    try {
      const res = await GetAllSchedulesService(_date);
      if (res.success && res.data) {
        setSchedule(res.data);
      } else throw new Error(res.error);
    } catch (e) {
      toast.show({
        text: ExceptionDetails('Schedule failed to load', e),
        type: 'error',
      });
    } finally {
      stopLoading();
    }
  };

  const createAppointment = async (dto: TAppointmentForm) => {
    startLoading();
    const dtoNoStatus = {
      ...dto,
      status: undefined,
    };

    try {
      const res = await CreateAppointmentService(dtoNoStatus);
      if (res.success && res.data) {
        toast.show({ text: 'Appointment created.', type: 'success' });
      } else throw new Error(res.error);
    } catch (e) {
      toast.show({
        text: ExceptionDetails('Failed to schedule the appointment.', e),
        type: 'error',
      });
      throw e;
    } finally {
      stopLoading();
    }
  };

  const rescheduleAppointment = async (
    uuid: string,
    dto: RescheduleAppointmentDTO
  ) => {
    startLoading();
    try {
      const res = await RescheduleAppointmentService(uuid, dto);
      if (res.success) {
        toast.show({ text: 'Appointment rescheduled.', type: 'success' });
      } else throw new Error(res.error);
    } catch (e) {
      toast.show({
        text: ExceptionDetails('Failed to reschedule the appointment.', e),
        type: 'error',
      });
      throw e;
    } finally {
      stopLoading();
    }
  };

  const cancelAppointment = async (uuid: string) => {
    startLoading();
    try {
      const res = await CancelAppointmentService(uuid);
      if (res.success) {
        toast.show({ text: 'Appointment canceled.', type: 'success' });
      } else throw new Error(res.error);
    } catch (e) {
      toast.show({
        text: ExceptionDetails('Failed to cancel the appointment.', e),
        type: 'error',
      });
      throw e;
    } finally {
      stopLoading();
    }
  };

  const startAppointment = async (uuid: string) => {
    startLoading();
    try {
      const res = await StartAppointmentService(uuid);
      if (res.success) {
        toast.show({
          text: 'Appointment started. Examination started.',
          type: 'success',
        });
      } else throw new Error(res.error);
    } catch (e) {
      toast.show({
        text: ExceptionDetails('Failed to start the appointment.', e),
        type: 'error',
      });
      throw e;
    } finally {
      stopLoading();
    }
  };

  useEffect(() => {
    if (date) load(date);
  }, []);

  return {
    schedule,
    isLoading,
    load,
    createAppointment,
    rescheduleAppointment,
    cancelAppointment,
    startAppointment,
  };
}

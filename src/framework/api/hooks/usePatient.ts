import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import { TPatientProfileForm } from '../../../components/molecules/PatientProfileForm';
import { useLoading } from '../../hooks/useLoading';
import { Patient } from '../../models/Patient';
import { useToastStore } from '../../store/ToastStore';
import { Uuid } from '../../types/Uuid';
import { VoidType } from '../../types/VoidType';
import { ExceptionDetails } from '../../utils/ExceptionDetails';
import { PatientAPIService } from '../services/PatientServices';

export function usePatient(uuid?: string) {
  const { isLoading, startLoading, stopLoading } = useLoading();
  const [patient, setPatient] = useState<Patient>();
  const toast = useToastStore();
  const navigate = useNavigate();

  const load = async (_uuid: string) => {
    setPatient(undefined);
    startLoading();
    try {
      const res = await PatientAPIService.getOne(_uuid);
      if (res.success && res.data) {
        setPatient(res.data);
        console.log('patient name', res.data.name);
      } else throw new Error(res.error);
    } catch (e) {
      toast.show({ text: 'Failed to load the patient.', type: 'error' });
    } finally {
      stopLoading();
    }
  };

  const update = async (_uuid: string, patient: TPatientProfileForm) => {
    startLoading();
    try {
      const res = await PatientAPIService.update(_uuid, patient, VoidType);
      if (res.success) {
        toast.show({
          text: 'Patient saved.',
          type: 'success',
        });
      } else throw new Error(res.error);
    } catch (e) {
      toast.show({
        text: ExceptionDetails('Failed to save the patient.', e),
        type: 'error',
      });
    } finally {
      stopLoading();
    }
  };

  const create = async (patient: TPatientProfileForm) => {
    startLoading();
    try {
      const res = await PatientAPIService.create(patient, Uuid);
      stopLoading();
      if (res.success && res.data) {
        toast.show({
          text: 'Patient created.',
          type: 'success',
        });
        navigate({ pathname: `../${res.data.value}` });
      } else throw new Error(res.error);
    } catch (e) {
      toast.show({
        text: ExceptionDetails('Failed to create the patient.', e),
        type: 'error',
      });
    } finally {
      stopLoading();
    }
  };

  useEffect(() => {
    if (uuid) load(uuid);
  }, []);

  return { patient, update, create, load, isLoading };
}

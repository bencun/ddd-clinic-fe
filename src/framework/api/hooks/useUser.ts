import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import { TUserProfileForm } from '../../../components/molecules/UserProfileForm';
import { useLoading } from '../../hooks/useLoading';
import { User } from '../../models/User';
import { useToastStore } from '../../store/ToastStore';
import { ExceptionDetails } from '../../utils/ExceptionDetails';
import { AdminRequestPasswordResetService } from '../services/ActivationServices';
import { EmailDTO } from '../services/dtos/EmailDTO';
import {
  CreateUserService,
  DeleteUserService,
  GetUserService,
  UpdateUserService,
} from '../services/UserServices';

export function useUser(uuid?: string) {
  const [user, setUser] = useState<User>();
  const { isLoading, startLoading, stopLoading } = useLoading();
  const toast = useToastStore();
  const navigate = useNavigate();

  const load = async (_uuid: string) => {
    setUser(undefined);
    startLoading();
    try {
      const res = await GetUserService(_uuid);
      if (res.success && res.data) {
        setUser(res.data);
      } else throw new Error(res.error);
    } catch (e) {
      toast.show({
        text: ExceptionDetails('User failed to load.', e),
        type: 'error',
      });
    } finally {
      stopLoading();
    }
  };

  const update = async (_uuid: string, formData: TUserProfileForm) => {
    const formDataNoRole: Partial<TUserProfileForm> = {
      ...formData,
      role: undefined,
    };
    startLoading();
    try {
      const res = await UpdateUserService(
        _uuid,
        formDataNoRole as TUserProfileForm
      );
      if (res.success && res.data) {
        toast.show({
          text: 'User profile updated.',
          type: 'success',
        });
        setUser(res.data);
      } else throw new Error(res.error);
    } catch (e) {
      toast.show({
        text: ExceptionDetails('Failed to update the user.', e),
        type: 'error',
      });
    } finally {
      stopLoading();
    }
  };

  const create = async (formData: TUserProfileForm) => {
    startLoading();
    try {
      const res = await CreateUserService(formData);
      if (res.success && res.data) {
        toast.show({
          text: 'User profile created.',
          type: 'success',
        });
        navigate({ pathname: `../${res.data.uuid}` });
      } else throw new Error(res.error);
    } catch (e) {
      toast.show({
        text: ExceptionDetails('Failed to create the user.', e),
        type: 'error',
      });
    } finally {
      stopLoading();
    }
  };

  const requestPasswordReset = async (dto: EmailDTO) => {
    startLoading();
    try {
      const res = await AdminRequestPasswordResetService(dto);
      if (res.success) {
        toast.show({
          text: 'User password has been reset and email sent.',
          type: 'success',
        });
      } else throw new Error(res.error);
    } catch (e) {
      toast.show({
        text: ExceptionDetails('Failed to reset the user password.', e),
        type: 'error',
      });
    } finally {
      stopLoading();
    }
  };

  const deleteUser = async (uuid: string) => {
    startLoading();
    try {
      const res = await DeleteUserService(uuid);
      if (res.success) {
        toast.show({
          text: 'User has been deleted.',
          type: 'success',
        });
        navigate({ pathname: '../' });
      } else throw new Error(res.error);
    } catch (e) {
      toast.show({
        text: ExceptionDetails('Failed to delete the user.', e),
        type: 'error',
      });
    } finally {
      stopLoading();
    }
  };
  useEffect(() => {
    if (uuid) load(uuid);
  }, []);

  return {
    isLoading,
    user,
    load,
    update,
    create,
    requestPasswordReset,
    deleteUser,
  };
}

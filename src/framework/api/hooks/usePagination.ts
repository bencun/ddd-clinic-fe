import { useEffect, useState } from 'react';
import { useLoading } from '../../hooks/useLoading';
import {
  OrderingParams,
  PaginationQueryParams,
  PaginationResponseParamsDTO,
} from '../../types/PaginationParameterTypes';
import { ResponseWrapper } from '../../utils/APIService';

export type PaginationHookLoader<T> = (
  p: PaginationQueryParams
) => Promise<ResponseWrapper<T[]>>;

export type PaginationHook<T> = {
  list: T[];
  isLoading: boolean;
  error?: string;
  page: number;
  pages: number;
  hasNext: boolean;
  hasPrev: boolean;
  next: () => void;
  prev: () => void;
  setOrdering: (p: OrderingParams) => void;
  search: (t?: string) => void;
};

export function usePagination<T>(
  cb: PaginationHookLoader<T>
): PaginationHook<T> {
  /** data we need */
  const [list, setList] = useState<T[]>([]);
  const { isLoading, startLoading, stopLoading } = useLoading(false);
  const [error, setError] = useState<string>();
  const [count, setCount] = useState<number>(0);
  const [skip, setSkip] = useState<number>(0);
  const [take, setTake] = useState<number>(15);
  const [orderBy, setOrderBy] = useState<Partial<OrderingParams>>({});
  const [text, setSearchText] = useState<string | undefined>();

  /** helper method */
  const load = async (
    _skip: number,
    _orderBy: Partial<OrderingParams> = {},
    _text?: string
  ) => {
    setError(undefined);
    startLoading();
    try {
      const response = await cb({
        skip: _skip,
        take,
        text: _text ? _text.trim() : undefined,
        ..._orderBy,
      });
      if (response.success && response.data) {
        const newParams = response.body as PaginationResponseParamsDTO;
        setSkip(newParams.skip);
        setTake(newParams.take);
        setCount(newParams.count);
        setOrderBy(_orderBy);
        setSearchText(_text);
        setList(response.data);
      }
    } catch (e) {
      setError((e as Error).message);
      setList([]);
    } finally {
      stopLoading();
    }
  };

  /** initial load only */
  useEffect(() => {
    load(skip, orderBy, text);
  }, []);

  /** return values */
  const page = skip / take + 1;
  const pages = Math.ceil(count / take);
  const hasNext = page < pages && !isLoading;
  const hasPrev = page > 1 && !isLoading;
  const next = async () => {
    if (!isLoading && hasNext) {
      load(skip + take, orderBy, text);
    }
  };
  const prev = async () => {
    if (!isLoading && hasPrev) {
      load(skip - take, orderBy, text);
    }
  };
  const setOrdering = (orderBy: OrderingParams) => {
    if (!isLoading) {
      load(0, orderBy, text);
    }
  };

  const search = (t?: string) => {
    if (!isLoading) {
      load(0, orderBy, t);
    }
  };

  return {
    list,
    isLoading,
    error,
    page,
    pages,
    hasNext,
    hasPrev,
    next,
    prev,
    setOrdering,
    search,
  };
}

import { useEffect, useState } from 'react';
import { User } from '../../models/User';
import { useToastStore } from '../../store/ToastStore';
import { ExceptionDetails } from '../../utils/ExceptionDetails';
import { GetAllUsersService } from '../services/UserServices';

export function useUsers() {
  const [users, setUsers] = useState<User[]>([]);
  const toast = useToastStore();

  const load = async () => {
    setUsers([]);
    try {
      const res = await GetAllUsersService();
      if (res.success && res.data) {
        setUsers(res.data);
      } else throw new Error(res.error);
    } catch (e) {
      toast.show({
        text: ExceptionDetails('Failed to load the users list.', e),
        type: 'error',
      });
    }
  };

  useEffect(() => {
    load();
  }, []);

  return users;
}

export type PaginationResponseParamsDTO = {
  skip: number;
  take: number;
  count: number;
};

export type PaginationQueryParams = PaginationParams &
  Partial<OrderingParams> & {
    text?: string;
  };

export type PaginationParams = {
  skip: number;
  take: number;
};

export type OrderingParams = {
  orderBy?: string;
  direction: 'asc' | 'desc';
};

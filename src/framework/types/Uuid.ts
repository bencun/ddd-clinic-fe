export interface UuidDTO {
  uuid: string;
}

export class Uuid {
  public readonly value: string;
  constructor(dto: UuidDTO) {
    this.value = dto.uuid;
  }
}

export enum Role {
  ADMIN = 'ADMIN',
  NURSE = 'NURSE',
  PRACTITIONER = 'PRACTITIONER',
}

type TRoleLabel = {
  [x in Role]: string;
};

export const RoleLabel: TRoleLabel = {
  ADMIN: 'Administrator',
  NURSE: 'Nurse',
  PRACTITIONER: 'Practitioner',
};

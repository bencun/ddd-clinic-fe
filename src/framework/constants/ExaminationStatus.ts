export enum ExaminationStatus {
  IN_PROGRESS = 'IN_PROGRESS',
  COMPLETE = 'COMPLETE',
  CANCELED = 'CANCELED',
}

type TExaminationStatusLabel = {
  [x in ExaminationStatus]: string;
};

export const ExaminationStatusLabel: TExaminationStatusLabel = {
  IN_PROGRESS: 'In Progress',
  COMPLETE: 'Complete',
  CANCELED: 'Canceled',
};

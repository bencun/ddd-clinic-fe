export const REFRESH_URL = '/auth/refresh';
export const LOGIN_URL = '/auth/login';
export const LOGOUT_URL = '/auth/logout';

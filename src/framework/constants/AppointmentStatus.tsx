export enum AppointmentStatus {
  NONE = 'NONE',
  STARTED = 'STARTED',
  FINISHED = 'FINISHED',
  CANCELED = 'CANCELED',
}
type TAppointmentStatusLabel = {
  [x in AppointmentStatus]: string;
};

export const AppointmentStatusLabel: TAppointmentStatusLabel = {
  NONE: 'Scheduled',
  CANCELED: 'Canceled',
  FINISHED: 'Finished',
  STARTED: 'In Progress',
};

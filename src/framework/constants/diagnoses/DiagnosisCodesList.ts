import { DiagnosisCodesMap } from './DiagnosisCodesMap';

export const DiagnosisCodesList = Array.from(DiagnosisCodesMap).map(
  ([code, name]) => ({
    code,
    name,
  })
);

export const DiagnosisCodesSearchList = Array.from(DiagnosisCodesMap).map(
  ([code, name]) => ({
    value: { code, name },
    text: `${code} - ${name}`,
  })
);

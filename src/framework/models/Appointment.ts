import { parseISO } from 'date-fns';
import { AppointmentStatus } from '../constants/AppointmentStatus';

export interface AppointmentDTO {
  uuid: string;
  patientUuid: string;
  patientName: string;
  duration: number;
  start: string;
  status: string;
}

export class Appointment {
  public uuid: string;
  public patientUuid: string;
  public patientName: string;
  public duration: number;
  public start: Date;
  public status: AppointmentStatus;
  constructor(dto: AppointmentDTO) {
    this.uuid = dto.uuid;
    this.patientUuid = dto.patientUuid;
    this.patientName = dto.patientName;
    this.duration = dto.duration;
    this.start = parseISO(dto.start);
    this.status = dto.status as AppointmentStatus;
  }
}

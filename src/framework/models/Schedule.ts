import { Appointment, AppointmentDTO } from './Appointment';
import { parseISO } from 'date-fns';

export interface ScheduleDTO {
  date: string;
  roomsAvailable: number;
  appointments: AppointmentDTO[];
}

export class Schedule {
  public readonly date: Date;
  public readonly roomsAvailable: number;
  public appointments: Appointment[];
  constructor(dto: ScheduleDTO) {
    this.date = parseISO(dto.date);
    this.roomsAvailable = dto.roomsAvailable;
    this.appointments = dto.appointments.map((d) => new Appointment(d));
  }
}

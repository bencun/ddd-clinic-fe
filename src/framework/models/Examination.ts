import { PrescriptionDTO } from './HealthRecord';
import { parseISO } from 'date-fns';

export interface ExaminationDTO {
  uuid: string;
  appointmentUuid: string;
  practitionerUuid: string;
  patientUuid: string;
  patientName: string;
  startedAt: string;
  endedAt?: string;
  knownChronicDiagnoses: string[];
  status: string;
  examinationData?: ExaminationDataDTO;
}

export interface ExaminationDataDTO {
  anamnesis: string;
  diagnoses: string[];
  chronicDiagnosesEstablished: string[];
  chronicDiagnosesRemoved: string[];
  prescriptions: PrescriptionDTO[];
  notes?: string;
}

export class Examination {
  public uuid: string;
  public appointmentUuid: string;
  public practitionerUuid: string;
  public patientUuid: string;
  public patientName: string;
  public startedAt: Date;
  public endedAt?: Date;
  public knownChronicDiagnoses: string[];
  public status: string;
  public examinationData?: ExaminationDataDTO;

  constructor(dto: ExaminationDTO) {
    this.uuid = dto.uuid;
    this.appointmentUuid = dto.appointmentUuid;
    this.practitionerUuid = dto.practitionerUuid;
    this.patientUuid = dto.patientUuid;
    this.patientName = dto.patientName;
    this.startedAt = parseISO(dto.startedAt);
    this.endedAt = dto.endedAt ? parseISO(dto.endedAt) : undefined;
    this.knownChronicDiagnoses = dto.knownChronicDiagnoses;
    this.status = dto.status;
    this.examinationData = dto.examinationData
      ? new ExaminationData(dto.examinationData)
      : undefined;
  }
}

export class ExaminationData {
  public anamnesis: string;
  public diagnoses: string[];
  public chronicDiagnosesEstablished: string[];
  public chronicDiagnosesRemoved: string[];
  public prescriptions: PrescriptionDTO[];
  public notes?: string;

  constructor(dto: ExaminationDataDTO) {
    this.anamnesis = dto.anamnesis;
    this.diagnoses = dto.diagnoses;
    this.chronicDiagnosesEstablished = dto.chronicDiagnosesEstablished;
    this.chronicDiagnosesRemoved = dto.chronicDiagnosesRemoved;
    this.prescriptions = dto.prescriptions;
    this.notes = dto.notes;
  }
}

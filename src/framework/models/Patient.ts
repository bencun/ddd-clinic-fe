import { HealthRecord, HealthRecordDTO } from './HealthRecord';

export interface PatientBasicDTO {
  uuid: string;
  name: string;
  jmbg: string;
  lbo: string;
  email: string;
  phone: string;
  city: string;
  postalCode: string;
  street: string;
  number: string;
  healthRecords: HealthRecordDTO[];
}

export class Patient {
  public uuid: string;
  public name: string;
  public jmbg: string;
  public lbo: string;
  public email: string;
  public phone: string;
  public city: string;
  public postalCode: string;
  public street: string;
  public number: string;
  public healthRecords: HealthRecord[];

  constructor(dto: PatientBasicDTO) {
    this.uuid = dto.uuid;
    this.name = dto.name;
    this.jmbg = dto.jmbg;
    this.lbo = dto.lbo;
    this.email = dto.email;
    this.phone = dto.phone;
    this.city = dto.city;
    this.postalCode = dto.postalCode;
    this.street = dto.street;
    this.number = dto.number;
    this.healthRecords = dto.healthRecords.map((hr) => new HealthRecord(hr));
  }
}

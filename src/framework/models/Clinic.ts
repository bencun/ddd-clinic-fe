export type ClinicPreferenceKeys =
  | 'CLINIC_NAME'
  | 'CLINIC_ADDRESS'
  | 'CLINIC_PHONE_NUMBER'
  | 'CLINIC_EMAIL'
  | 'CLINIC_WEBSITE'
  | 'CLINIC_NUMBER_OF_ROOMS';

export interface ClinicDTO {
  preferences: PreferenceDTO[];
}

export interface PreferenceDTO {
  key: ClinicPreferenceKeys;
  value: number | string;
}

export class Clinic {
  public readonly preferences = new Map<ClinicPreferenceKeys, unknown>();
  constructor(dto: ClinicDTO) {
    dto.preferences.forEach((p) => this.preferences.set(p.key, p.value));
  }

  get PreferencesArray() {
    return [...this.preferences.entries()].map(([key, value]) => ({
      key,
      value,
    }));
  }
}

import { Role } from '../types/Role';

export class User {
  public uuid: string;
  public email: string;
  public name: string;
  public phone: string;
  public role: Role;
  public deleted: boolean;
  public createdAt: Date;
  public updatedAt: Date;
  public activated: boolean;

  constructor(dto: {
    uuid: string;
    email: string;
    name: string;
    phone: string;
    role: Role;
    deleted: boolean;
    createdAt: Date;
    updatedAt: Date;
    activated: boolean;
  }) {
    this.uuid = dto.uuid;
    this.email = dto.email;
    this.name = dto.name;
    this.phone = dto.phone;
    this.role = dto.role;
    this.deleted = dto.deleted;
    this.createdAt = dto.createdAt;
    this.updatedAt = dto.updatedAt;
    this.activated = dto.activated;
  }
}

import { parseISO } from 'date-fns';

export interface HealthRecordDTO {
  uuid: string;
  date: string;
  examinationUuid: string;
  diagnosesEstablished: string[];
  chronicDiagnosesRemoved: string[];
  chronicDiagnosesEstablished: string[];
  prescriptions: PrescriptionDTO[];
}

export interface PrescriptionDTO {
  notes: string;
  medication: MedicationDTO;
}

export interface MedicationDTO {
  name: string;
  spec: SpecDTO;
}

export interface SpecDTO {
  doseAmount: number;
  doseUnit: string;
}

export class HealthRecord {
  public uuid: string;
  public date: Date;
  public examinationUuid: string;
  public diagnosesEstablished: string[];
  public chronicDiagnosesRemoved: string[];
  public chronicDiagnosesEstablished: string[];
  public prescriptions: PrescriptionDTO[];
  constructor(dto: HealthRecordDTO) {
    this.uuid = dto.uuid;
    this.date = parseISO(dto.date);
    this.examinationUuid = dto.examinationUuid;
    this.diagnosesEstablished = dto.diagnosesEstablished;
    this.chronicDiagnosesRemoved = dto.chronicDiagnosesRemoved;
    this.chronicDiagnosesEstablished = dto.chronicDiagnosesEstablished;
    this.prescriptions = dto.prescriptions;
  }
}

import { useEffect } from 'react';
import { useNavigate } from 'react-router';
import { DomainEventToken } from '../constants/DomainEventToken';
import { useAuthStore } from '../store/AuthStore';
import { SocketService } from '../utils/SocketService';

export function setupSockets() {
  const { loggedIn } = useAuthStore(({ loggedIn, user }) => ({
    loggedIn,
    user,
  }));
  const navigate = useNavigate();

  // Global socket control
  useEffect(() => {
    if (loggedIn) {
      SocketService.connect();
    } else {
      SocketService.disconnect();
    }
  }, [loggedIn]);

  // global subscriptions
  SocketService.subscribe(DomainEventToken.APPOINTMENT_STARTED, () => {
    navigate({ pathname: '/examinations' });
  });
}

import { useState } from 'react';

export const useLoading = (defaultValue = false) => {
  const [isLoading, setLoading] = useState<boolean>(defaultValue);

  return {
    isLoading,
    startLoading: () => {
      setLoading(true);
    },
    stopLoading: () => {
      setLoading(false);
    },
  };
};

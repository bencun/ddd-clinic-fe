import { useEffect } from 'react';
import { SocketService, TListenerFunction } from '../utils/SocketService';

export function useSocket(event: string, callback: TListenerFunction) {
  useEffect(() => {
    const subscription = SocketService.subscribe(event, callback);

    return () => {
      SocketService.unsubscribe(subscription);
    };
  });
}

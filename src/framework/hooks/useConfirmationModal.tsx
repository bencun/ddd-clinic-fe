import { useState } from 'react';
import { ConfirmationModal } from '../../components/organisms/ConfirmationModal';

export function useConfirmationModal() {
  const [open, setOpen] = useState(false);
  const [cb, setCb] = useState<() => void>();

  const show = (cb: () => void) => {
    setCb(() => cb);
    setOpen(true);
  };
  const hide = () => {
    setOpen(false);
    setCb(undefined);
  };

  const confirmationModal = (
    <ConfirmationModal
      no={hide}
      yes={() => {
        cb && cb();
        hide();
      }}
      open={open}
    />
  );

  return {
    confirmationModal,
    show,
    hide,
  };
}

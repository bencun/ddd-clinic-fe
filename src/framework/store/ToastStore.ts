import { StoreWithDevtools } from './StoreWithDevtools';

type ToastType = 'info' | 'success' | 'error';

interface IToast {
  type: ToastType;
  text: string;
  duration: number;
  action?: {
    text: string;
    callback: () => void;
  };
  timeout?: NodeJS.Timeout;
}

interface IToastStore {
  list: IToast[];
  show: (t: Partial<IToast>) => IToast;
  hide: (t: IToast) => void;
}

export const useToastStore = StoreWithDevtools<IToastStore>((set, get) => ({
  list: [],
  show: ({
    text = '',
    type = 'info',
    duration = 5,
    action,
  }: Partial<IToast>) => {
    const toast: IToast = { text, type, duration, action };
    const timeoutHandler = setTimeout(() => {
      get().hide(toast);
    }, duration * 1000);
    toast.timeout = timeoutHandler;
    const newList = [...get().list];
    newList.push(toast);
    set(() => ({ list: newList }));
    return toast;
  },
  hide: (t: IToast) => {
    if (t.timeout) clearTimeout(t.timeout);
    const newList = get().list.filter((x) => x !== t);
    set(() => ({ list: newList }));
  },
}));

import { devtools } from 'zustand/middleware';
import create, { State, StateCreator } from 'zustand';

export const StoreWithDevtools = <T extends State>(state: StateCreator<T>) =>
  create(devtools(state));

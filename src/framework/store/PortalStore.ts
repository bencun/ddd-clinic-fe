import { ReactNode } from 'react';
import ReactDOM from 'react-dom';
import { StoreWithDevtools } from './StoreWithDevtools';

interface IPortalStore {
  element?: Element | null;
  setElement: () => void;
}

export const usePortalStore = StoreWithDevtools<IPortalStore>((set) => ({
  setElement: () =>
    set(() => ({ element: document.querySelector('#nav-portal') })),
}));

export function usePortal(children: ReactNode) {
  const target = usePortalStore((s) => s.element);
  if (!target) return null;
  return ReactDOM.createPortal(children, target);
}

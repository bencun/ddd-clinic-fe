import {
  AuthLoginService,
  AuthLogoutService,
  AuthRefreshService,
} from '../api/services/AuthServices';
import { GetClinicPreferencesService } from '../api/services/ClinicServices';
import { Clinic } from '../models/Clinic';
import { User } from '../models/User';
import { VoidType } from '../types/VoidType';
import { StoreWithDevtools } from './StoreWithDevtools';

type LoginDTO = {
  email: string;
  password: string;
};

interface AuthState {
  initialized: boolean;
  loggedIn: boolean;
  user?: User;
  clinic?: Clinic;
  initialize: () => Promise<void>;
  login: (data: LoginDTO) => Promise<void>;
  logout: () => Promise<void>;
}

export const useAuthStore = StoreWithDevtools<AuthState>((set, get) => ({
  initialized: false,
  loggedIn: false,
  initialize: async () => {
    const refreshRes = await AuthRefreshService.get({}, User);
    if (refreshRes.success && refreshRes.data) {
      const clinicRes = await GetClinicPreferencesService();
      if (clinicRes.success && clinicRes.data) {
        set(() => ({
          initialized: true,
          user: refreshRes.data,
          loggedIn: true,
          clinic: clinicRes.data,
        }));
        return;
      }
    }
    set(() => ({ initialized: true, loggedIn: false }));
  },
  login: async (data: LoginDTO) => {
    const loginRes = await AuthLoginService.post(data, User);
    if (loginRes.success && loginRes.data) {
      const clinicRes = await GetClinicPreferencesService();
      if (clinicRes.success && clinicRes.data) {
        set(() => ({
          initialized: true,
          user: loginRes.data,
          loggedIn: true,
          clinic: clinicRes.data,
        }));
        return;
      }
    } else {
      set(() => ({ loggedIn: false, user: undefined }));
    }
  },
  logout: async () => {
    if (get().loggedIn) {
      const req = await AuthLogoutService.get({}, VoidType);
      if (req.success) {
        set(() => ({ loggedIn: false, user: undefined }));
      }
    } else {
      set(() => ({ loggedIn: false, user: undefined }));
    }
  },
}));

import { http } from './http';
import { ParamObject } from './ParamObject';
import { Type } from '../types/Type';
import { PaginationQueryParams } from '../types/PaginationParameterTypes';

export function MapArray<T>(array: unknown[], MapperClass: Type<T>): T[] {
  return array.map((e) => new MapperClass(e));
}

export type ResponseWrapper<DataType> = {
  success: boolean;
  error?: string;
  response: Response;
  body?: unknown;
  data?: DataType;
};

export class APIService<T> {
  constructor(
    private readonly url: string,
    public readonly MapperClass: Type<T>
  ) {}

  private async ParseResponse<R>(response: Response, mapperClass: Type<R>) {
    const wrapper: ResponseWrapper<R> = {
      success: false,
      response,
    };

    if (response.ok) {
      wrapper.success = true;
      try {
        const parsedBody = await response.json();
        wrapper.body = parsedBody;
        if (Array.isArray(parsedBody)) {
          // check whether it's an array
          wrapper.data = MapArray(parsedBody, mapperClass) as unknown as R;
        } else if (Array.isArray(parsedBody?.data)) {
          // check whether embedded data is an array (for pagination)
          wrapper.data = MapArray(parsedBody.data, mapperClass) as unknown as R;
        } else {
          // or just an object
          wrapper.data = new mapperClass(parsedBody);
        }
      } catch (e) {
        wrapper.data = undefined;
      }
    } else {
      wrapper.success = false;
      try {
        const parsedBody = await response.json();
        wrapper.body = parsedBody;
        if (typeof parsedBody?.message === 'string') {
          wrapper.error = parsedBody.message;
        }
        if (Array.isArray(parsedBody.message)) {
          wrapper.error = (parsedBody.message as string[]).join(';');
        }
      } catch (e) {
        wrapper.data = undefined;
      }
    }
    return wrapper;
  }

  /** Mapped get request with T response data type */
  async getOne<P extends ParamObject>(
    id: string,
    params?: P
  ): Promise<ResponseWrapper<T>> {
    return this.ParseResponse<T>(
      await http.get(`${this.url}/${id}`, params),
      this.MapperClass
    );
  }

  /** Mapped get all request with T response data type */
  async getAll<P extends Partial<PaginationQueryParams>>(
    params: P
  ): Promise<ResponseWrapper<T[]>> {
    return this.ParseResponse<T[]>(
      await http.get(this.url, params),
      this.MapperClass as unknown as Type<T[]>
    );
  }
  /** Raw get request with R response data type */
  async get<D extends ParamObject, R>(
    params: D,
    mapperClass: Type<R>
  ): Promise<ResponseWrapper<R>> {
    return this.ParseResponse<R>(await http.get(this.url, params), mapperClass);
  }

  /** Raw get request with R response data type */
  async post<D, R>(data: D, mapperClass: Type<R>): Promise<ResponseWrapper<R>> {
    return this.ParseResponse<R>(
      await http.post<D>(this.url, data),
      mapperClass
    );
  }

  /** Raw post request with R response data type */
  async create<D, R>(
    data: D,
    mapperClass: Type<R>
  ): Promise<ResponseWrapper<R>> {
    return this.ParseResponse<R>(
      await http.post<D>(this.url, data),
      mapperClass
    );
  }

  async update<D, R>(
    id: string,
    data: D,
    mapperClass: Type<R>
  ): Promise<ResponseWrapper<R>> {
    return this.ParseResponse<R>(
      await http.put<D>(this.url, data, id),
      mapperClass
    );
  }

  async patch<D, R>(
    id: string,
    mapperClass: Type<R>,
    data?: D
  ): Promise<ResponseWrapper<R>> {
    return this.ParseResponse<R>(
      await http.patch<D>(this.url, data, id),
      mapperClass
    );
  }

  async delete<R>(
    id: string,
    mapperClass: Type<R>
  ): Promise<ResponseWrapper<R>> {
    return this.ParseResponse<R>(await http.delete(this.url, id), mapperClass);
  }

  suffix<M>(url: string, mapperClass: Type<M>) {
    return new APIService(`${this.url}/${url}`, mapperClass);
  }
}

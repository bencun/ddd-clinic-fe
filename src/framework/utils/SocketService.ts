import { io } from 'socket.io-client';

export type TListenerFunction = (data: unknown) => void;
export type TSubscription = { event: string; callback: TListenerFunction };

function subscribe(
  event: string,
  callback: TListenerFunction
): Readonly<TSubscription> {
  const subscription: TSubscription = { event, callback } as const;
  socket.on(event, callback);
  return subscription;
}

function unsubscribe(subscription: Readonly<TSubscription>) {
  socket.off(subscription.event, subscription.callback);
}

// socket
const socket = io('http://localhost:3000', { withCredentials: true });

socket.on('connect', () => {
  console.log('Connected!', socket.id);
});

socket.onAny((...data) => {
  console.log('Got some data!!!', data);
});

socket.on('connect_error', () => {
  console.log('Connection error!', socket.id, socket.auth);
});

socket.on('disconnect', (reason) => {
  console.log('Disconnected!', socket.id, reason, typeof reason);
});

const connect = () => {
  if (!socket.connected) socket.connect();
};

const disconnect = () => {
  if (socket.connected) socket.disconnect();
};

export const SocketService = {
  connect,
  disconnect,
  subscribe,
  unsubscribe,
} as const;

import { ParamObject } from './ParamObject';
import { RefreshWrapper } from './RefreshWrapper';

const credentials: RequestInit = {
  credentials: 'include',
};

const headers = {
  'Content-Type': 'application/json',
};

function objectToQueryString(paramsObject: ParamObject, prefix?: string) {
  const finalString: string[] = [];
  for (const key in paramsObject) {
    if (paramsObject[key]) {
      const prefixedKey = prefix ? prefix + '[' + key + ']' : key;
      const value = paramsObject[key];
      finalString.push(
        value !== null && typeof value === 'object'
          ? objectToQueryString(value, prefixedKey)
          : encodeURIComponent(prefixedKey) +
              '=' +
              encodeURIComponent(value || '')
      );
    }
  }
  return finalString.join('&');
}

export const http = {
  get(url: string, params?: ParamObject) {
    if (params) {
      const query = objectToQueryString(params);
      url += query ? `?${query}` : '';
    }
    return RefreshWrapper(url, { ...credentials });
  },
  post<D>(url: string, data: D) {
    const options = {
      method: 'POST',
      headers,
      body: JSON.stringify(data),
    };
    return RefreshWrapper(url, { ...options, ...credentials });
  },
  put<D>(url: string, data: D, id?: string) {
    const options = {
      method: 'PUT',
      headers,
      body: JSON.stringify(data),
    };
    return RefreshWrapper(`${url}${id ? `/${id}` : ''}`, {
      ...options,
      ...credentials,
    });
  },
  delete(url: string, id?: string) {
    const options = {
      method: 'DELETE',
    };
    return RefreshWrapper(`${url}${id ? `/${id}` : ''}`, {
      ...options,
      ...credentials,
    });
  },
  patch<D>(url: string, data?: D, id?: string) {
    const options = {
      method: 'PATCH',
      headers,
      body: data ? JSON.stringify(data) : undefined,
    };
    return RefreshWrapper(`${url}${id ? `/${id}` : ''}`, {
      ...options,
      ...credentials,
    });
  },
};

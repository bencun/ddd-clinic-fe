export function ExceptionDetails(message: string, e: unknown) {
  if (!e) return message;
  if ((e as Error)?.message) {
    return `${message} ${
      (e as Error).message ? `Details: ${(e as Error).message}` : ''
    }`;
  }
  return message;
}

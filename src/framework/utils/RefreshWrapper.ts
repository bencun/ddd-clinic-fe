import { LOGIN_URL, REFRESH_URL } from '../constants/AuthURLs';
import { useAuthStore } from '../store/AuthStore';

const PrefixURL = (url: string) =>
  process.env.NODE_ENV === 'development' ? url : url;

export async function RefreshWrapper(
  url: string,
  options?: RequestInit
): Promise<Response> {
  const response = await fetch(PrefixURL(url), options);
  /**
   * try refreshing the token ONLY if this req isn't:
   * - hitting the refresh endpoint
   * - hitting the login endpoint
   * - and response code is 401
   * */
  if (url !== REFRESH_URL && url !== LOGIN_URL && response.status === 401) {
    const refresh = await fetch(PrefixURL(url), { credentials: 'include' });
    if (refresh.status === 401) {
      // logout
      useAuthStore.setState({ loggedIn: false, user: undefined });
      return refresh;
    } else {
      return await fetch(PrefixURL(url), options);
    }
  } else {
    return response;
  }
}

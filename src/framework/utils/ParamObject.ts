export type ParamObject = Record<
  string,
  string | number | undefined | Record<string, string>
>;

import React from 'react';
import { useSchedule } from '../../framework/api/hooks/useSchedule';
import {
  AppointmentForm,
  TAppointmentForm,
} from '../molecules/AppointmentForm';
import { Modal } from '../molecules/Modal';

export interface ICreateAppointmentModalProps {
  open: boolean;
  close: () => void;
  start: Date;
  duration: number;
  onCreated?: () => void;
}
export const CreateAppointmentModal: React.FC<ICreateAppointmentModalProps> = ({
  close,
  start,
  duration,
  onCreated,
}) => {
  const { createAppointment } = useSchedule();

  const createAppointmentProxy = (p: TAppointmentForm) => {
    createAppointment(p).then(
      () => {
        onCreated && onCreated();
        close();
      },
      () => {
        /*  */
      }
    );
  };

  return (
    <Modal open={true} onClose={close} header={<h3>New Appointment</h3>}>
      {
        <AppointmentForm
          onSubmit={(p) => createAppointmentProxy(p)}
          onCancel={close}
          defaults={{
            start,
            duration,
          }}
          submitText="Schedule"
        />
      }
    </Modal>
  );
};

import {
  PaginationHookLoader,
  usePagination,
} from '../../framework/api/hooks/usePagination';
import { PatientAPIService } from '../../framework/api/services/PatientServices';
import { Patient } from '../../framework/models/Patient';
import { ListItem } from '../atoms/ListItem';
import { PaginationController } from '../molecules/PaginationController';

const loader: PaginationHookLoader<Patient> = async (p) => {
  return await PatientAPIService.getAll(p);
};

interface IPatientListProps {
  select: (p: Patient) => void;
}

export const PatientsList: React.FC<IPatientListProps> = ({ select }) => {
  const paginationHook = usePagination(loader);
  const { list } = paginationHook;
  const paginationController = (
    <PaginationController
      hook={paginationHook}
      hideNavigation
      className="ml-2"
    />
  );

  return (
    <>
      <div>{paginationController}</div>
      <div className="overflow-auto w-96 h-96 flex flex-wrap">
        {list.map((p) => (
          <ListItem
            key={p.uuid}
            header={
              <>
                <div className="font-bold">{p.name}</div>
                <div className="text-sm">
                  LBO: {p.lbo}, JMBG: {p.jmbg}
                </div>
              </>
            }
            footer={
              <div>
                <button
                  type="button"
                  className="btn btn-sm mr-4"
                  onClick={() => select(p)}
                >
                  Select
                </button>
              </div>
            }
          >
            <div className="flex flex-col text-sm">
              <div>City: {p.city}</div>
              <div>Email: {p.email}</div>
              <div>Phone: {p.phone}</div>
            </div>
          </ListItem>
        ))}
      </div>
    </>
  );
};

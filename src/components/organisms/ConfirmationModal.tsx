import React from 'react';
import { Modal } from '../molecules/Modal';

interface IConfirmationModal {
  open: boolean;
  yes: () => void;
  no: () => void;
}
export const ConfirmationModal: React.FC<IConfirmationModal> = ({
  open,
  yes,
  no,
}) => {
  return (
    <Modal
      open={open}
      onClose={no}
      header={<h3>Are you sure?</h3>}
      footer={
        <div className="flex justify-between">
          <button className="btn btn-warning" onClick={yes}>
            Yes
          </button>
          <button className="btn btn-outline" onClick={no}>
            No
          </button>
        </div>
      }
    ></Modal>
  );
};

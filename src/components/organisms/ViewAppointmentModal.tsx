import React from 'react';
import { useSchedule } from '../../framework/api/hooks/useSchedule';
import { AppointmentStatus } from '../../framework/constants/AppointmentStatus';
import { Appointment } from '../../framework/models/Appointment';
import { useAuthStore } from '../../framework/store/AuthStore';
import { Role } from '../../framework/types/Role';
import { VoidMethod } from '../../framework/types/VoidMethod';
import {
  AppointmentForm,
  TAppointmentForm,
} from '../molecules/AppointmentForm';
import { Modal } from '../molecules/Modal';
import { AuthGuard } from '../utils/AuthGuard';

export interface IViewAppointmentModalProps {
  appointment: Appointment;
  close: () => void;
  onUpdated?: () => void;
}
export const ViewAppointmentModal: React.FC<IViewAppointmentModalProps> = ({
  close,
  appointment,
  onUpdated,
}) => {
  const { user } = useAuthStore();
  const { rescheduleAppointment, cancelAppointment, startAppointment } =
    useSchedule();

  const rescheduleAppointmentProxy = ({
    start,
    duration,
  }: TAppointmentForm) => {
    rescheduleAppointment(appointment.uuid, { start, duration })
      .then(() => {
        onUpdated && onUpdated();
        close();
      })
      .catch(VoidMethod);
  };

  const cancelAppointmentProxy = () => {
    cancelAppointment(appointment.uuid)
      .then(() => {
        onUpdated && onUpdated();
        close();
      })
      .catch(VoidMethod);
  };

  const startAppointmentProxy = () => {
    startAppointment(appointment.uuid)
      .then(() => {
        onUpdated && onUpdated();
        close();
      })
      .catch(VoidMethod);
  };

  return (
    <Modal open={true} onClose={close} header={<h3>View Appointment</h3>}>
      {
        <AppointmentForm
          onSubmit={rescheduleAppointmentProxy}
          onCancel={close}
          appointment={appointment}
          submitText="Reschedule"
          disabled={
            appointment.status !== AppointmentStatus.NONE ||
            user?.role !== Role.NURSE
          }
          actions={
            <>
              {appointment.status === AppointmentStatus.NONE && (
                <>
                  <AuthGuard roles={[Role.NURSE]}>
                    <button
                      type="button"
                      className="btn btn-sm btn-warning mr-2"
                      onClick={cancelAppointmentProxy}
                    >
                      Cancel Appointment
                    </button>
                  </AuthGuard>
                  <AuthGuard roles={[Role.PRACTITIONER]}>
                    <button
                      type="button"
                      className="btn btn-sm mr-2"
                      onClick={startAppointmentProxy}
                    >
                      Start Appointment
                    </button>
                  </AuthGuard>
                </>
              )}
            </>
          }
        />
      }
    </Modal>
  );
};

import React, { useCallback, useMemo, useState } from 'react';
import { Appointment } from '../../framework/models/Appointment';
import { Schedule } from '../../framework/models/Schedule';
import {
  addDays,
  startOfDay,
  endOfDay,
  isWithinInterval,
  isBefore,
  format,
  setHours,
  setMinutes,
  setSeconds,
  areIntervalsOverlapping,
  roundToNearestMinutes,
  setYear,
  setMonth,
  setDay,
  getDay,
  getMonth,
  getYear,
} from 'date-fns';
import { WorkingHours } from '../../framework/constants/WorkingHours';
import { addMinutes } from 'date-fns/esm';
import classNames from 'classnames';
import { AppointmentStatus } from '../../framework/constants/AppointmentStatus';
import {
  CreateAppointmentModal,
  ICreateAppointmentModalProps,
} from './CreateAppointmentModal';
import {
  IViewAppointmentModalProps,
  ViewAppointmentModal,
} from './ViewAppointmentModal';
import { useAuthStore } from '../../framework/store/AuthStore';
import { Role } from '../../framework/types/Role';

type AppointmentTrack = {
  appointments: Appointment[];
};
type WeekdayAppointments = {
  start: Date;
  tracks: AppointmentTrack[];
};

interface IScheduleCalendarProps {
  schedule: Schedule;
  roomsAvailable: number;
}
export const ScheduleCalendar: React.FC<IScheduleCalendarProps> = ({
  schedule,
  roomsAvailable,
}) => {
  // auth
  const { user } = useAuthStore();
  // mouse position stuff
  const [mousePos, setMousePos] = useState<{
    x: number;
    y: number;
    date: string;
    rawDate: Date;
    rawX: number;
    rawY: number;
  }>();
  const onMouseEnterOrMove = (
    e: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => {
    const { height, top, left } = (
      document.querySelector('.tracks-container') as Element
    ).getBoundingClientRect();
    const mouseY = e.pageY;
    const mouseYOffset = (mouseY - top) / height;
    // now that we got 0-100 offset we can calculate the actual datetime on the line itself
    if (mouseYOffset >= 0) {
      const workStart = setHours(
        setMinutes(setSeconds(schedule.date, 0), 0),
        WorkingHours.Start
      );
      const workEnd = setHours(
        setMinutes(setSeconds(schedule.date, 0), 0),
        WorkingHours.End
      );
      // find the date at offset
      const offsetTimeMsFactored =
        workStart.getTime() +
        (workEnd.getTime() - workStart.getTime()) * mouseYOffset;
      const date = roundToNearestMinutes(new Date(offsetTimeMsFactored), {
        nearestTo: 5,
      });
      // round to minutes
      setMousePos({
        x: 0,
        y: mouseYOffset * 100,
        rawDate: date,
        date: format(date, 'HH:mm'),
        rawX: e.pageX - left,
        rawY: e.pageY - top,
      });
    }
  };
  const onMouseLeave = () => {
    setMousePos(undefined);
  };

  // workflow
  const [createAppointmentModalData, setCreateAppointmentModalData] =
    useState<ICreateAppointmentModalProps>();
  const [viewAppointmentModalData, setViewAppointmentModalData] =
    useState<IViewAppointmentModalProps>();
  const startCreateAppointment = (start: Date) => {
    if (mousePos && user?.role === Role.NURSE) {
      setCreateAppointmentModalData({
        open: true,
        close: () => setCreateAppointmentModalData(undefined),
        duration: 15,
        start: setYear(
          setMonth(setDay(mousePos.rawDate, getDay(start)), getMonth(start)),
          getYear(start)
        ),
      });
    }
  };
  const startViewAppointment = (appointment: Appointment) => {
    if (mousePos) {
      setViewAppointmentModalData({
        close: () => setViewAppointmentModalData(undefined),
        appointment,
      });
    }
  };

  // memoize everything
  const weekdays = useMemo(() => {
    const { date: scheduleStartDate } = schedule;
    // sort schedules per weekdays
    const weekdays: WeekdayAppointments[] = [];
    for (let i = 0; i < 6; i++) {
      const dayStart = startOfDay(addDays(scheduleStartDate, i));
      const dayEnd = endOfDay(dayStart);

      // get all appointments for this day and sort them
      const dayAppointments = schedule.appointments
        .filter((s) =>
          isWithinInterval(s.start, {
            start: dayStart,
            end: dayEnd,
          })
        )
        .sort((a1, a2) => (isBefore(a1.start, a2.start) ? -1 : 1));

      // create at least N tracks to fit overlapping appointments
      const tracks: AppointmentTrack[] = Array(roomsAvailable)
        .fill(undefined)
        .map(() => ({ appointments: [] }));
      for (const appointment of dayAppointments) {
        let appointmentPlaced = false;
        for (const track of tracks) {
          const overlappingAppointmentsInTrack = track.appointments.filter(
            (trackAppointment) =>
              areIntervalsOverlapping(
                {
                  start: trackAppointment.start,
                  end: addMinutes(
                    trackAppointment.start,
                    trackAppointment.duration
                  ),
                },
                {
                  start: appointment.start,
                  end: addMinutes(appointment.start, appointment.duration),
                }
              )
          );
          if (overlappingAppointmentsInTrack.length === 0) {
            track.appointments.push(appointment);
            appointmentPlaced = true;
            break;
          }
        } // end for track
        // push a new track if no fitting track was found
        if (!appointmentPlaced) {
          tracks.push({ appointments: [appointment] });
        }
      } // end for appointment
      weekdays.push({ start: dayStart, tracks });
    } // end for days
    return weekdays;
  }, [schedule]);

  // offset calculation
  const calculateOffsetAndHeight = useCallback((appointment: Appointment) => {
    const { start } = appointment;
    const workStart = setHours(
      setMinutes(setSeconds(start, 0), 0),
      WorkingHours.Start
    );
    const workEnd = setHours(
      setMinutes(setSeconds(start, 0), 0),
      WorkingHours.End
    );
    const workdayDuration = workEnd.getTime() - workStart.getTime();
    const appointmentOffset = start.getTime() - workStart.getTime();
    return {
      offset: (appointmentOffset / workdayDuration) * 100,
      height: ((appointment.duration * 60) / (workdayDuration / 1000)) * 100,
    };
  }, []);

  // render
  return (
    <div className="w-full h-[calc(100vh-theme(space.24))] relative">
      {/* Modals */}
      {createAppointmentModalData && (
        <CreateAppointmentModal {...createAppointmentModalData} />
      )}
      {viewAppointmentModalData && (
        <ViewAppointmentModal {...viewAppointmentModalData} />
      )}
      {/* Tooltip for time*/}
      {mousePos && (
        <div
          className="absolute z-20 bg-white rounded-md p-2 ml-4 mt-1 w-18 border border-teal-700"
          style={{
            top: `${mousePos.rawY}px`,
            left: `${mousePos.rawX}px`,
          }}
        >
          {mousePos.date}
        </div>
      )}
      {/* Weekdays container */}
      <div
        className="flex h-full items-stretch"
        onMouseMove={onMouseEnterOrMove}
        onMouseLeave={onMouseLeave}
      >
        {weekdays.map((w, idx) => (
          /* Single day column */
          <div
            key={idx}
            className="flex flex-col w-1/6 min-w-[120px] bg-gray-200 mr-1 border rounded-md"
          >
            <div className="text-center">{format(w.start, 'eee dd.MM.')}</div>
            {/* Tracks container */}
            <div className="tracks-container h-full bg-gray-100 flex justify-between relative cursor-pointer">
              {/* Mouse line */}
              {mousePos && (
                <div
                  className="absolute h-[4px] w-full left-0 bg-teal-600 z-10"
                  style={{ top: `${mousePos.y}%` }}
                  onClick={() => startCreateAppointment(w.start)}
                ></div>
              )}
              {w.tracks.map((t, idx) => (
                /* Single track */
                <div key={`track-${idx}`} className="relative grow m-1">
                  {t.appointments.map((a) => (
                    /* Single appointment */
                    <div
                      key={a.uuid}
                      className={classNames(
                        'absolute w-full min-h-[40px] p-1 rounded-md border-t border-t-teal-500 cursor-pointer z-[15]',
                        a.status === AppointmentStatus.NONE && 'bg-teal-300',
                        a.status === AppointmentStatus.CANCELED &&
                          'bg-gray-300',
                        a.status === AppointmentStatus.FINISHED &&
                          'bg-blue-300',
                        a.status === AppointmentStatus.STARTED && 'bg-red-300'
                      )}
                      style={{
                        top: `${calculateOffsetAndHeight(a).offset}%`,
                        height: `${calculateOffsetAndHeight(a).height}%`,
                      }}
                      onClick={(e) => {
                        e.stopPropagation();
                        startViewAppointment(a);
                      }}
                    >
                      <div className="text-xs whitespace-nowrap text-ellipsis overflow-hidden">
                        {format(a.start, 'HH:mm')}-
                        {format(addMinutes(a.start, a.duration), 'HH:mm')}
                      </div>
                      <div className="text-xs whitespace-nowrap text-ellipsis overflow-hidden">
                        {a.patientName}
                      </div>
                    </div>
                  ))}
                </div>
              ))}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

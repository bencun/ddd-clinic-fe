import classNames from 'classnames';
import React from 'react';
import { useToastStore } from '../../framework/store/ToastStore';

export const ToastController: React.FC = () => {
  const { list, hide } = useToastStore(({ list, hide }) => ({
    list,
    hide,
  }));

  return (
    <>
      <div className="fixed bottom-0 right-4 w-96 text-base z-50">
        {list.map((t, idx) => {
          return (
            <div
              key={idx}
              className={classNames(
                'card pr-0 text-teal-50 relative flex justify-between items-center',
                t.type === 'info' && 'bg-blue-700',
                t.type === 'error' && 'bg-red-700',
                t.type === 'success' && 'bg-green-700'
              )}
            >
              <div>
                <div>{t.text}</div>
                {t.action ? (
                  <div
                    className="underline cursor-pointer"
                    onClick={() => {
                      hide(t);
                      t.action?.callback();
                    }}
                  >
                    {t.action.text}
                  </div>
                ) : null}
              </div>
              <button className="px-4">&times;</button>
            </div>
          );
        })}
      </div>
    </>
  );
};

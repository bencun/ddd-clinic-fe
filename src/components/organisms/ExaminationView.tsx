import { Examination } from '../../framework/models/Examination';
import * as yup from 'yup';
import { useFieldArray, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { InputWrapper } from '../atoms/InputWrapper';
import { usePortal } from '../../framework/store/PortalStore';
import { PrescriptionDTO } from '../../framework/models/HealthRecord';
import { DiagnosisCodesMap } from '../../framework/constants/diagnoses/DiagnosisCodesMap';
import { AutofillField } from '../molecules/AutofillField';
import { DiagnosisCodesSearchList } from '../../framework/constants/diagnoses/DiagnosisCodesList';
import { useMemo } from 'react';
import { useConfirmationModal } from '../../framework/hooks/useConfirmationModal';

export type TExaminationForm = {
  anamnesis: string;
  diagnoses: { code: string; name: string }[];
  chronicDiagnosesEstablished: { code: string; name: string }[];
  chronicDiagnosesRemoved: { code: string; name: string }[];
  prescriptions: PrescriptionDTO[];
  notes?: string;
};

const ExaminationFormSchema = yup.object().shape({
  anamnesis: yup.string().required(),
  diagnoses: yup.array().of(
    yup.object().shape({
      name: yup.string(),
      code: yup.string().required(),
    })
  ),
  chronicDiagnosesEstablished: yup.array().of(
    yup.object().shape({
      name: yup.string(),
      code: yup.string().required(),
    })
  ),
  chronicDiagnosesRemoved: yup.array().of(
    yup.object().shape({
      name: yup.string(),
      code: yup.string().required(),
    })
  ),
  notes: yup.string(),
  prescriptions: yup.array().of(
    yup.object().shape({
      notes: yup.string().required(),
      medication: yup
        .object()
        .required()
        .shape({
          name: yup.string().required(),
          spec: yup
            .object()
            .shape({
              doseAmount: yup.number().moreThan(0).required(),
              doseUnit: yup.string().oneOf(['mg', 'ml', 'g']).required(),
            })
            .required(),
        }),
    })
  ),
});

export interface IExaminationView {
  examination: Examination;
  onSubmit: (e: TExaminationForm) => void;
  onCancel: (e: TExaminationForm) => void;
}
export const ExaminationView: React.FC<IExaminationView> = ({
  examination,
  onSubmit,
  onCancel,
}) => {
  const { confirmationModal, show } = useConfirmationModal();
  const completeExamination = () => {
    show(handleSubmit(onSubmit));
  };
  const cancelExamination = () => {
    show(handleSubmit(onCancel));
  };

  const { examinationData } = examination;
  // form hook
  const {
    handleSubmit,
    register,
    formState: { errors },
    control,
    getValues,
  } = useForm<TExaminationForm>({
    resolver: yupResolver(ExaminationFormSchema),
    defaultValues: examinationData
      ? {
          anamnesis: examinationData.anamnesis,
          chronicDiagnosesEstablished:
            examinationData.chronicDiagnosesEstablished.map((code) => ({
              code,
              name: DiagnosisCodesMap.get(code),
            })),
          chronicDiagnosesRemoved: examinationData.chronicDiagnosesRemoved.map(
            (code) => ({
              code,
              name: DiagnosisCodesMap.get(code),
            })
          ),
          diagnoses: examinationData.diagnoses.map((code) => ({
            code,
            name: DiagnosisCodesMap.get(code),
          })),
          notes: examinationData.notes,
          prescriptions: [...examinationData.prescriptions],
        }
      : {
          chronicDiagnosesEstablished: [],
          chronicDiagnosesRemoved: [],
          diagnoses: [],
          notes: '',
          prescriptions: [],
        },
  });

  const knownChronicDiagnosesMapped = useMemo(
    () =>
      examination.knownChronicDiagnoses.map((code) => ({
        code,
        name: DiagnosisCodesMap.get(code),
      })),
    [examination.knownChronicDiagnoses]
  );

  const knownChronicDiagnosesMappedForSearch = useMemo(
    () =>
      knownChronicDiagnosesMapped.map((d) => ({
        text: `${d.code} - ${d.name}`,
        value: d,
      })),
    [examination.knownChronicDiagnoses]
  );

  const {
    fields: prescriptionFields,
    prepend: prependPrescription,
    remove: removePrescription,
  } = useFieldArray({
    name: 'prescriptions',
    control,
  });

  const {
    fields: diagnosesFields,
    append: appendDiagnosis,
    remove: removeDiagnosis,
  } = useFieldArray({
    name: 'diagnoses',
    control,
  });

  const {
    fields: chDiagnosesEstFields,
    append: appendChDiagnosisEst,
    remove: removeChDiagnosisEst,
  } = useFieldArray({
    name: 'chronicDiagnosesEstablished',
    control,
  });

  const {
    fields: chDiagnosesRemFields,
    append: appendChDiagnosisRem,
    remove: removeChDiagnosisRem,
  } = useFieldArray({
    name: 'chronicDiagnosesRemoved',
    control,
  });

  const actions = (
    <>
      <button
        className="btn btn-sm btn-outline mr-2"
        onClick={completeExamination}
      >
        Complete Examination
      </button>
      <button className="btn btn-sm btn-warning" onClick={cancelExamination}>
        Abort Examination
      </button>
    </>
  );

  const navPortal = usePortal(actions);

  return (
    <form className="p-4">
      {confirmationModal}
      <h2>Anamnesis</h2>
      <InputWrapper label="Patient" error={errors.anamnesis?.message}>
        <textarea {...register('anamnesis')} rows={10}></textarea>
      </InputWrapper>

      {/* Diagnoses */}
      <h2>Diagnoses Established</h2>
      <div className="flex flex-col">
        <div>
          <AutofillField
            label="Search for Diagnoses to Add to this Anamnesis"
            items={DiagnosisCodesSearchList}
            exclude={getValues().diagnoses}
            valueKey="code"
            onSelect={(d) => {
              appendDiagnosis(d);
            }}
          />
        </div>

        <div>
          {diagnosesFields.map((field, idx) => (
            <div key={field.id} className="flex items-center">
              <InputWrapper
                label={`Diagnosis #${idx + 1}`}
                error={errors.diagnoses?.[idx].code?.message}
              >
                <input
                  className="pb-2"
                  type="text"
                  value={field.code}
                  disabled
                />
                <input type="text" value={field.name} disabled />
              </InputWrapper>
              <button
                type="button"
                className="btn btn-sm btn-warning"
                onClick={() => removeDiagnosis(idx)}
              >
                Remove Diagnosis
              </button>
            </div>
          ))}
        </div>
      </div>

      <h2>Prescriptions</h2>
      <p>Add recommended prescriptions to this examination.</p>
      <button
        type="button"
        className="btn mb-2"
        onClick={() =>
          prependPrescription({
            notes: '',
            medication: { name: '', spec: { doseAmount: 1, doseUnit: 'mg' } },
          })
        }
      >
        Add Prescription
      </button>
      <div>
        {prescriptionFields.map((p, idx) => (
          <div key={p.id} className="mb-4">
            <h3>Medication #{idx}</h3>
            <InputWrapper
              label={'Medication Name'}
              error={errors.prescriptions?.[idx]?.medication?.name?.message}
            >
              <input
                className="pb-2"
                type="text"
                {...register(`prescriptions.${idx}.medication.name`)}
              />
            </InputWrapper>
            <InputWrapper
              label={'Prescription Instructions'}
              error={errors.prescriptions?.[idx]?.notes?.message}
            >
              <textarea
                className="pb-2"
                {...register(`prescriptions.${idx}.notes`)}
              ></textarea>
            </InputWrapper>
            <h3>Medication Dosage</h3>
            <InputWrapper
              label={'Amount'}
              error={
                errors.prescriptions?.[idx]?.medication?.spec?.doseAmount
                  ?.message
              }
            >
              <input
                className="pb-2"
                type="number"
                min="1"
                max="100000"
                {...register(`prescriptions.${idx}.medication.spec.doseAmount`)}
              />
            </InputWrapper>
            <InputWrapper
              label={'Unit'}
              error={
                errors.prescriptions?.[idx]?.medication?.spec?.doseUnit?.message
              }
            >
              <select
                className="pb-2"
                {...register(`prescriptions.${idx}.medication.spec.doseUnit`)}
              >
                <option value="mg">mg</option>
                <option value="g">g</option>
                <option value="ml">ml</option>
              </select>
            </InputWrapper>
            <button
              type="button"
              className="btn btn-warning"
              onClick={() => removePrescription(idx)}
            >
              Remove Prescription
            </button>
          </div>
        ))}
      </div>

      {/* New Chronic Diagnoses */}
      <h2>Newly Established Chronic Diagnoses</h2>
      <p>Permanently add new chronic diagnoses to the health record.</p>
      <div className="flex flex-col">
        <div>
          <AutofillField
            label="Search for Chronic Diagnoses to Establish in the health record"
            items={DiagnosisCodesSearchList}
            exclude={getValues().chronicDiagnosesEstablished}
            valueKey="code"
            onSelect={(d) => {
              appendChDiagnosisEst(d);
            }}
          />
        </div>

        <div>
          {chDiagnosesEstFields.map((field, idx) => (
            <div key={field.id} className="flex items-center">
              <InputWrapper
                label={`Diagnosis #${idx + 1}`}
                error={errors.diagnoses?.[idx].code?.message}
              >
                <input
                  className="pb-2"
                  type="text"
                  value={field.code}
                  disabled
                />
                <input type="text" value={field.name} disabled />
              </InputWrapper>
              <button
                type="button"
                className="btn btn-sm btn-warning"
                onClick={() => removeChDiagnosisEst(idx)}
              >
                Remove Diagnosis
              </button>
            </div>
          ))}
        </div>
      </div>

      {/* Remove Chronic Diagnoses */}
      <h2>Remove Previously Known Chronic Diagnoses</h2>
      <p>
        Currently in the health record:
        {knownChronicDiagnosesMappedForSearch.length > 0
          ? knownChronicDiagnosesMappedForSearch.map((d) => d.text).join(',')
          : ' none.'}
      </p>
      <p>
        You can choose an established chronic diagnosis to remove from the
        health record.
      </p>
      <div className="flex flex-col">
        <div>
          <AutofillField
            label="Search for Chronic Diagnoses to Remove from the health record"
            items={knownChronicDiagnosesMappedForSearch}
            exclude={getValues().chronicDiagnosesRemoved}
            valueKey="code"
            onSelect={(d) => {
              appendChDiagnosisRem(d);
            }}
          />
        </div>

        <div>
          {chDiagnosesRemFields.map((field, idx) => (
            <div key={field.id} className="flex items-center">
              <InputWrapper
                label={`Diagnosis #${idx + 1}`}
                error={errors.diagnoses?.[idx].code?.message}
              >
                <input
                  className="pb-2"
                  type="text"
                  value={field.code}
                  disabled
                />
                <input type="text" value={field.name} disabled />
              </InputWrapper>
              <button
                type="button"
                className="btn btn-sm btn-warning"
                onClick={() => removeChDiagnosisRem(idx)}
              >
                Keep Diagnosis
              </button>
            </div>
          ))}
        </div>
      </div>

      <h2>Other Notes</h2>
      <InputWrapper label="Other Notes" error={errors.notes?.message}>
        <textarea {...register('notes')} rows={10}></textarea>
      </InputWrapper>

      {navPortal}
    </form>
  );
};

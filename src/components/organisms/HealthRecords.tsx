import React, { useState } from 'react';
import { HealthRecord } from '../../framework/models/HealthRecord';
import { HealthRecordCard } from '../molecules/HealthRecordCard';

interface IHealthRecordsProps {
  healthRecords: HealthRecord[];
  show?: boolean;
}
export const HealthRecords: React.FC<IHealthRecordsProps> = ({
  healthRecords,
  show,
}) => {
  const [internalShow, setInternalShow] = useState<boolean>(show || false);

  const iconElement = internalShow ? <span>Hide</span> : <span>Show</span>;

  return (
    <div className="w-full flex flex-col items-stretch">
      <div className="bg-gray-300 rounded-tl-lg rounded-tr-lg p-4">
        {' '}
        <button
          className="w-full text-left"
          onClick={() => setInternalShow(!internalShow)}
        >
          {iconElement} Health Records
        </button>
      </div>
      {internalShow && (
        <div className="bg-gray-100 rounded-bl-lg rounded-br-lg">
          {healthRecords.map((hr) => (
            <HealthRecordCard key={hr.uuid} healthRecord={hr} />
          ))}
        </div>
      )}
    </div>
  );
};

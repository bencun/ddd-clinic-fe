import { useMemo, useState } from 'react';
import { string } from 'yup';
import { boolean } from 'yup/lib/locale';
import { InputWrapper } from '../atoms/InputWrapper';

export type TSearchItem<T> = {
  text: string;
  value: T;
};

export interface IAutofillFieldProps<T> {
  label: string;
  onSelect: (v: T) => void;
  items: TSearchItem<T>[];
  exclude: T[];
  valueKey: keyof T;
}

type ComponentProps<T> = React.PropsWithChildren<IAutofillFieldProps<T>>;

export const AutofillField = <T extends object | string>({
  label,
  items,
  exclude,
  onSelect,
  valueKey,
}: ComponentProps<T>) => {
  const [searchText, setSearchText] = useState<string>('');
  const [values, setValues] = useState<IAutofillFieldProps<T>['items']>();

  const filteredItems = useMemo(() => {
    return items.filter(
      (item) => !exclude.find((e) => e[valueKey] === item.value[valueKey])
    );
  }, [items, exclude]);

  const search = (v: string) => {
    setSearchText(v);
    const trimmedValue = v.trim();
    if (trimmedValue.length > 0) {
      setValues(
        filteredItems
          .filter((i) => i.text.toLowerCase().includes(v.toLowerCase()))
          .slice(0, 25)
      );
    } else {
      setValues([]);
    }
  };

  const selectValue = (value: T) => {
    onSelect(value);
    search('');
  };

  return (
    <div className="flex flex-col relative">
      <InputWrapper label={label} className="px-0">
        <input
          type="search"
          value={searchText}
          onInput={(e: React.ChangeEvent<HTMLInputElement>) =>
            search(e.target.value)
          }
        ></input>
      </InputWrapper>
      {values && values.length > 0 && (
        <div className="absolute top-20 left-2 right-2 max-h-64 overflow-x-auto border rounded-lg border-teal-700 z-[25] bg-white">
          {values?.map((v, idx) => (
            <div
              className="bg-gray-100 hover:bg-gray-300 p-1 cursor-pointer"
              key={idx}
              onClick={() => selectValue(v.value)}
            >
              {v.text}
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

import React, { ReactNode } from 'react';
import ReactDOM from 'react-dom';

interface IModalProps {
  open: boolean;
  onClose: () => void;
  header?: ReactNode;
  footer?: ReactNode;
}
export const Modal: React.FC<IModalProps> = ({
  children,
  onClose,
  open,
  header,
  footer,
}) => {
  const portal = ReactDOM.createPortal(
    <div
      className="z-[45] fixed inset-0 bg-black/20 flex justify-center items-center"
      onClick={onClose}
    >
      <div
        className="card relative max-h-screen overflow-auto"
        onClick={(e) => e.stopPropagation()}
      >
        <button onClick={onClose} className="absolute top-0 right-0 p-2">
          &times;
        </button>
        {header && <div className="p-4">{header}</div>}
        <div>{children}</div>
        {footer && <div className="p-4">{footer}</div>}
      </div>
    </div>,
    document.querySelector('#modal-portal') as Element
  );

  if (open) return portal;
  return null;
};

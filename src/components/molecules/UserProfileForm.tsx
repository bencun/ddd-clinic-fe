import React, { ReactNode } from 'react';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { InputWrapper } from '../atoms/InputWrapper';
import { useNavigate } from 'react-router';
import { usePortal } from '../../framework/store/PortalStore';
import { Role, RoleLabel } from '../../framework/types/Role';
import { User } from '../../framework/models/User';

export type TUserProfileForm = {
  email: string;
  name: string;
  phone: string;
  role: Role;
};

const ProfileFormSchema = yup.object().shape({
  name: yup.string().required(),
  email: yup.string().required().email(),
  phone: yup.string().required(),
  role: yup.string().required().oneOf(Object.values(Role)),
});

interface IUserProfileForm {
  onSubmit: (p: TUserProfileForm) => void;
  user?: User;
  disabled?: boolean;
  actions?: ReactNode;
}

export const UserProfileForm: React.FC<IUserProfileForm> = ({
  user,
  onSubmit,
  disabled,
  actions,
}) => {
  const navigate = useNavigate();

  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<TUserProfileForm>({
    resolver: yupResolver(ProfileFormSchema),
    defaultValues: user
      ? {
          name: user.name,
          email: user.email,
          phone: user.phone,
          role: user.role,
        }
      : {},
  });

  const actionsPortal = usePortal(
    <>
      <button
        className="btn btn-sm btn-outline mr-2"
        onClick={handleSubmit(onSubmit)}
        disabled={disabled}
      >
        Save
      </button>
      <button
        className="btn btn-sm"
        onClick={() => navigate({ pathname: '/users' })}
        disabled={disabled}
      >
        Cancel
      </button>
      {actions || null}
    </>
  );

  return (
    <form className="flex flex-wrap">
      <InputWrapper label="Name" error={errors.name?.message}>
        <input type="text" {...register('name')} />
      </InputWrapper>

      <InputWrapper
        label="Email"
        error={errors.email?.message}
        className="w-1/2"
      >
        <input type="text" {...register('email')} disabled={!!user} />
      </InputWrapper>

      <InputWrapper
        label="Phone"
        error={errors.phone?.message}
        className="w-1/2"
      >
        <input type="text" {...register('phone')} />
      </InputWrapper>

      <InputWrapper label="Role" error={errors.role?.message}>
        <select {...register('role')} disabled={!!user}>
          {Object.values(Role).map((k) => {
            return (
              <option key={k} value={k}>
                {RoleLabel[k]}
              </option>
            );
          })}
        </select>
      </InputWrapper>

      {actionsPortal}
    </form>
  );
};

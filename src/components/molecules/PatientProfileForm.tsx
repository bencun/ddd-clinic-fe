import React from 'react';
import { Patient, PatientBasicDTO } from '../../framework/models/Patient';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { InputWrapper } from '../atoms/InputWrapper';
import { useNavigate } from 'react-router';
import { usePortal } from '../../framework/store/PortalStore';
import { Role } from '../../framework/types/Role';
import { AuthGuard } from '../utils/AuthGuard';
import { useAuthStore } from '../../framework/store/AuthStore';

export type TPatientProfileForm = Omit<
  PatientBasicDTO,
  'uuid' | 'healthRecords'
>;

const ProfileFormSchema = yup.object().shape({
  name: yup.string().required(),
  email: yup.string().required().email(),
  phone: yup.string().required(),
  lbo: yup.string().required().length(11),
  jmbg: yup.string().required().length(13),
  city: yup.string().required(),
  postalCode: yup.string().required(),
  street: yup.string().required(),
  number: yup.string().required(),
});

interface IPatientProfileForm {
  onSubmit: (p: TPatientProfileForm) => void;
  patient?: Patient;
  disabled?: boolean;
}

export const PatientProfileForm: React.FC<IPatientProfileForm> = ({
  patient,
  onSubmit,
  disabled,
}) => {
  const navigate = useNavigate();
  const readOnly = useAuthStore((s) => s.user)?.role !== Role.NURSE;

  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<TPatientProfileForm>({
    resolver: yupResolver(ProfileFormSchema),
    defaultValues: patient
      ? {
          name: patient.name,
          jmbg: patient.jmbg,
          lbo: patient.lbo,
          email: patient.email,
          phone: patient.phone,
          city: patient.city,
          postalCode: patient.postalCode,
          street: patient.street,
          number: patient.number,
        }
      : {},
  });

  const actions = usePortal(
    <AuthGuard roles={[Role.NURSE]}>
      <button
        className="btn btn-sm btn-outline mr-2"
        onClick={handleSubmit(onSubmit)}
        disabled={disabled}
      >
        Save
      </button>
      <button
        className="btn btn-sm"
        onClick={() => navigate({ pathname: '/patients' })}
        disabled={disabled}
      >
        Cancel
      </button>
    </AuthGuard>
  );

  return (
    <form className="flex flex-wrap">
      <InputWrapper label="Name" error={errors.name?.message}>
        <input type="text" {...register('name')} readOnly={readOnly} />
      </InputWrapper>

      <InputWrapper label="LBO" error={errors.lbo?.message}>
        <input type="text" {...register('lbo')} readOnly={readOnly} />
      </InputWrapper>

      <InputWrapper label="JMBG" error={errors.jmbg?.message}>
        <input type="text" {...register('jmbg')} readOnly={readOnly} />
      </InputWrapper>

      <InputWrapper
        label="Email"
        error={errors.email?.message}
        className="w-1/2"
      >
        <input type="text" {...register('email')} readOnly={readOnly} />
      </InputWrapper>

      <InputWrapper
        label="Phone"
        error={errors.phone?.message}
        className="w-1/2"
      >
        <input type="text" {...register('phone')} readOnly={readOnly} />
      </InputWrapper>

      <InputWrapper label="City" error={errors.city?.message} className="w-1/2">
        <input type="text" {...register('city')} readOnly={readOnly} />
      </InputWrapper>

      <InputWrapper
        label="Postal Code"
        error={errors.postalCode?.message}
        className="w-1/2"
      >
        <input type="text" {...register('postalCode')} readOnly={readOnly} />
      </InputWrapper>

      <InputWrapper
        label="Street"
        error={errors.street?.message}
        className="w-2/3"
      >
        <input type="text" {...register('street')} readOnly={readOnly} />
      </InputWrapper>

      <InputWrapper
        label="Number"
        error={errors.number?.message}
        className="w-1/3"
      >
        <input type="text" {...register('number')} readOnly={readOnly} />
      </InputWrapper>
      {actions}
    </form>
  );
};

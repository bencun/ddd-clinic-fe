import classNames from 'classnames';
import { PaginationHook } from '../../framework/api/hooks/usePagination';
import debounce from 'lodash/debounce';
import React from 'react';

interface IPaginationControllerProps {
  hook: PaginationHook<unknown>;
  className?: string;
  hideNavigation?: boolean;
  hideSearch?: boolean;
}
export const PaginationController: React.FC<IPaginationControllerProps> = ({
  hook,
  className,
  hideNavigation,
  hideSearch,
}) => {
  const handleInput = debounce((e: React.ChangeEvent<HTMLInputElement>) => {
    hook.search(e.target.value);
  }, 500);

  const navigation = (
    <>
      <button
        className="btn btn-outline btn-xs px-3 mr-2"
        disabled={!hook.hasPrev}
        onClick={hook.prev}
      >
        &lt;
      </button>
      {hook.pages === 0 && hook.isLoading ? (
        <span className="text-teal-50">N/A</span>
      ) : (
        <span className="text-teal-50">
          {hook.page}/{hook.pages}
        </span>
      )}
      <button
        className="btn btn-outline btn-xs px-3 ml-2"
        disabled={!hook.hasNext}
        onClick={hook.next}
      >
        &gt;
      </button>
    </>
  );

  const search = (
    <>
      <input
        type="text"
        className="xs ml-2"
        onChange={handleInput}
        placeholder="Search"
      />
    </>
  );
  return (
    <div className={classNames(className)}>
      {!hideNavigation && navigation}
      {!hideSearch && search}
    </div>
  );
};

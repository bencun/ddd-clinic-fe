import format from 'date-fns/format';
import React from 'react';
import { Link } from 'react-router-dom';
import { DiagnosisCodesMap } from '../../framework/constants/diagnoses/DiagnosisCodesMap';
import { HealthRecord } from '../../framework/models/HealthRecord';

interface IHealthRecordCardProps {
  healthRecord: HealthRecord;
}
export const HealthRecordCard: React.FC<IHealthRecordCardProps> = ({
  healthRecord,
}) => {
  const {
    examinationUuid,
    date,
    chronicDiagnosesEstablished,
    chronicDiagnosesRemoved,
    diagnosesEstablished,
    prescriptions,
  } = healthRecord;
  return (
    <div className="card my-4 shadow-none relative">
      {/* <Link
        to={`/examinations/${examinationUuid}`}
        className="absolute top-2 right-4 text-teal-700 underline"
      >
        Examination
      </Link> */}

      <p>Created on {format(date, "yyyy-MM-dd 'at' HH:mm")}</p>
      <p className="font-semibold">Diagnoses Established:</p>
      <p>
        {diagnosesEstablished.map((d, idx) => (
          <span key={idx} className="mr-1">{`${d}: ${DiagnosisCodesMap.get(
            d
          )};`}</span>
        ))}
      </p>
      {chronicDiagnosesRemoved.length > 0 && (
        <>
          <p className="font-semibold">Prescriptions:</p>

          {prescriptions.map((p, idx) => (
            <div key={idx}>
              <p>
                {p.medication.name}, {p.medication.spec.doseAmount}{' '}
                {p.medication.spec.doseUnit}
                {p.notes?.length > 0 && <span>; Notes: {p.notes}</span>}
              </p>
            </div>
          ))}
        </>
      )}
      {chronicDiagnosesEstablished.length > 0 && (
        <>
          <p className="font-semibold">Chronic Diagnoses Established:</p>
          <p>
            {chronicDiagnosesEstablished.map((d, idx) => (
              <span key={idx} className="mr-1">{`${d}: ${DiagnosisCodesMap.get(
                d
              )};`}</span>
            ))}
          </p>
        </>
      )}
      {chronicDiagnosesRemoved.length > 0 && (
        <>
          <p className="font-semibold">Chronic Diagnoses Removed:</p>
          <p>
            {chronicDiagnosesRemoved.map((d, idx) => (
              <span key={idx} className="mr-1">{`${d}: ${DiagnosisCodesMap.get(
                d
              )};`}</span>
            ))}
          </p>
        </>
      )}
    </div>
  );
};

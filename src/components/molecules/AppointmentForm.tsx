import React, { ReactNode } from 'react';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { InputWrapper } from '../atoms/InputWrapper';
import { Appointment } from '../../framework/models/Appointment';
import format from 'date-fns/format';
import {
  AppointmentStatus,
  AppointmentStatusLabel,
} from '../../framework/constants/AppointmentStatus';
import { PatientsList } from '../organisms/PatientsList';

const FormatDateForDatetimePicker = (date: Date) =>
  format(date, "yyyy-MM-dd'T'HH:mm:ss");

export type TAppointmentForm = {
  patientUuid: string;
  patientName: string;
  duration: number;
  start: string;
  status: string;
};

const ProfileFormSchema = yup.object().shape({
  patientUuid: yup.string().required(),
  patientName: yup.string().required(),
  duration: yup.number().required().min(5).max(60),
  start: yup.string().required(),
});

interface IAppointmentForm {
  onSubmit: (p: TAppointmentForm) => void;
  onCancel: () => void;
  submitText: string;
  appointment?: Appointment;
  disabled?: boolean;
  actions?: ReactNode;
  defaults?: {
    start: Date;
    duration: number;
  };
}

export const AppointmentForm: React.FC<IAppointmentForm> = ({
  appointment,
  onSubmit,
  onCancel,
  submitText,
  disabled,
  actions,
  defaults,
}) => {
  const {
    handleSubmit,
    register,
    formState: { errors },
    setValue, // use to set the patient uuid here
  } = useForm<TAppointmentForm>({
    resolver: yupResolver(ProfileFormSchema),
    defaultValues: appointment
      ? {
          patientUuid: appointment.patientUuid,
          patientName: appointment.patientName,
          start: FormatDateForDatetimePicker(appointment.start),
          duration: appointment.duration,
          status: appointment.status,
        }
      : defaults
      ? {
          start: FormatDateForDatetimePicker(defaults.start),
          duration: defaults.duration,
        }
      : {},
  });

  const localActions = (
    <>
      {actions || null}
      <button
        className="btn btn-sm mr-2"
        onClick={handleSubmit(onSubmit)}
        disabled={disabled}
      >
        {submitText}
      </button>
      <button className="btn btn-sm btn-outline" onClick={onCancel}>
        Cancel
      </button>
    </>
  );

  return (
    <form className="flex flex-col">
      <div className="flex">
        <div>
          {!appointment && (
            <PatientsList
              select={(p) => {
                setValue('patientUuid', p.uuid);
                setValue('patientName', p.name);
              }}
            />
          )}
        </div>
        <div>
          <InputWrapper label="Patient" error={errors.patientName?.message}>
            <input type="text" {...register('patientName')} disabled />
          </InputWrapper>
          <InputWrapper label="Start" error={errors.start?.message}>
            <input
              type="datetime-local"
              {...register('start')}
              disabled={disabled}
            />
          </InputWrapper>

          <InputWrapper
            label="Duration (in minutes)"
            error={errors.duration?.message}
          >
            <input
              type="number"
              {...register('duration')}
              min="5"
              max="60"
              step="5"
              disabled={disabled}
            />
          </InputWrapper>

          {appointment?.status && (
            <InputWrapper label="Status" error={errors.status?.message}>
              <select {...register('status')} disabled>
                {Object.values(AppointmentStatus).map((k) => (
                  <option key={k} value={k}>
                    {AppointmentStatusLabel[k]}
                  </option>
                ))}
              </select>
            </InputWrapper>
          )}
        </div>
      </div>

      <div className="w-full flex justify-end items-center mt-4">
        {localActions}
      </div>
    </form>
  );
};

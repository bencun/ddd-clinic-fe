import React, { ReactNode } from 'react';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { InputWrapper } from '../atoms/InputWrapper';
import { useNavigate } from 'react-router';
import { usePortal } from '../../framework/store/PortalStore';
import { Clinic, ClinicPreferenceKeys } from '../../framework/models/Clinic';
export type TClinicForm = {
  [x in ClinicPreferenceKeys]: string | number | any;
};

const PreferencesFormShema = yup.object().shape({
  CLINIC_NAME: yup.string().required(),
  CLINIC_ADDRESS: yup.string().required(),
  CLINIC_PHONE_NUMBER: yup.string().required(),
  CLINIC_EMAIL: yup.string().email().required(),
  CLINIC_WEBSITE: yup.string().url().required(),
  CLINIC_NUMBER_OF_ROOMS: yup.number().required().min(1).max(100),
});

interface IClinicFormProps {
  clinic?: Clinic;
  onSubmit: (p: TClinicForm) => void;
  disabled?: boolean;
  actions?: ReactNode;
}

export const ClinicForm: React.FC<IClinicFormProps> = ({
  clinic,
  onSubmit,
  disabled,
  actions,
}) => {
  const navigate = useNavigate();

  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<TClinicForm>({
    resolver: yupResolver(PreferencesFormShema),
    defaultValues: clinic
      ? {
          CLINIC_NAME: clinic.preferences.get('CLINIC_NAME') || '',
          CLINIC_NUMBER_OF_ROOMS:
            clinic.preferences.get('CLINIC_NUMBER_OF_ROOMS') || '',
          CLINIC_ADDRESS: clinic.preferences.get('CLINIC_ADDRESS') || '',
          CLINIC_EMAIL: clinic.preferences.get('CLINIC_EMAIL') || '',
          CLINIC_PHONE_NUMBER:
            clinic.preferences.get('CLINIC_PHONE_NUMBER') || '',
          CLINIC_WEBSITE: clinic.preferences.get('CLINIC_WEBSITE') || '',
        }
      : {},
  });

  const actionsPortal = usePortal(
    <>
      <button
        className="btn btn-sm btn-outline mr-2"
        onClick={handleSubmit(onSubmit)}
        disabled={disabled}
      >
        Save
      </button>
      <button
        className="btn btn-sm"
        onClick={() => navigate({ pathname: '/users' })}
        disabled={disabled}
      >
        Cancel
      </button>
      {actions || null}
    </>
  );

  return (
    <form className="flex flex-wrap">
      <InputWrapper label="Name" error={errors?.CLINIC_NAME?.message}>
        <input type="text" {...register('CLINIC_NAME')} />
      </InputWrapper>

      <InputWrapper
        label="Number of Examination Rooms"
        error={errors?.CLINIC_NUMBER_OF_ROOMS?.message}
      >
        <input type="text" {...register('CLINIC_NUMBER_OF_ROOMS')} />
      </InputWrapper>

      <InputWrapper
        label="Clinic Address"
        error={errors?.CLINIC_ADDRESS?.message}
      >
        <input type="text" {...register('CLINIC_ADDRESS')} />
      </InputWrapper>

      <InputWrapper
        label="Phone Number"
        error={errors?.CLINIC_PHONE_NUMBER?.message}
      >
        <input type="text" {...register('CLINIC_PHONE_NUMBER')} />
      </InputWrapper>

      <InputWrapper
        label="Email Addresss"
        error={errors?.CLINIC_EMAIL?.message}
      >
        <input type="text" {...register('CLINIC_EMAIL')} />
      </InputWrapper>

      <InputWrapper
        label="Website Address"
        error={errors?.CLINIC_WEBSITE?.message}
      >
        <input type="text" {...register('CLINIC_WEBSITE')} />
      </InputWrapper>

      {actionsPortal}
    </form>
  );
};

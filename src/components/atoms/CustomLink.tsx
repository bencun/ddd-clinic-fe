import classNames from 'classnames';
import React from 'react';
import { Link, LinkProps, useMatch, useResolvedPath } from 'react-router-dom';

interface ICustomLinkProps extends LinkProps {
  activeClassName?: string;
  end?: boolean;
}

export const CustomLink: React.FC<ICustomLinkProps> = ({
  children,
  to,
  className,
  activeClassName,
  end = false,
  ...props
}) => {
  const resolved = useResolvedPath(to);
  const match = useMatch({ path: resolved.pathname, end });

  return (
    <Link
      className={classNames(match && activeClassName, className)}
      to={to}
      {...props}
    >
      {children}
    </Link>
  );
};

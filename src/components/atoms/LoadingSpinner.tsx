export const LoadingSpinner: React.FC = () => (
  <div className="animate-spin">X</div>
);

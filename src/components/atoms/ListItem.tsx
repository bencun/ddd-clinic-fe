import classNames from 'classnames';
import { ReactElement } from 'react';

interface IListItemProps {
  header?: ReactElement;
  footer?: ReactElement;
  className?: string;
}

export const ListItem: React.FC<IListItemProps> = ({
  children,
  header,
  footer,
  className,
}) => {
  return (
    <div className={classNames('card', className)}>
      {header ? (
        <div className="border-b border-gray-200 pb-2 mb-2">{header}</div>
      ) : null}
      {children}
      {footer ? (
        <div className="border-t border-gray-200 pt-2 mt-2">{footer}</div>
      ) : null}
    </div>
  );
};

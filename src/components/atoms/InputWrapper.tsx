import classNames from 'classnames';
import React from 'react';

interface IInputWrapperProps {
  label: string;
  error?: string;
  className?: string;
}

export const InputWrapper: React.FC<IInputWrapperProps> = ({
  children,
  label,
  error,
  className,
}) => {
  return (
    <div
      className={classNames(
        'relative flex flex-col items-stretch mb-2 w-full px-2',
        className
      )}
    >
      <label className="text-base mb-1 font-semibold">{label}</label>
      {children}
      <div className="text-red-500 text-xs h-5 overflow-hidden max-w-full whitespace-nowrap">
        {error || ''}
      </div>
    </div>
  );
};

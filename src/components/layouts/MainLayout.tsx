import { useEffect } from 'react';
import { Outlet } from 'react-router';
import { useAuthStore } from '../../framework/store/AuthStore';
import { usePortalStore } from '../../framework/store/PortalStore';
import { Role } from '../../framework/types/Role';
import { CustomLink } from '../atoms/CustomLink';
import { AuthGuard } from '../utils/AuthGuard';

const navUrls: { label: string; path: string; roles: Role[] }[] = [
  { label: 'Users', path: '/users', roles: [Role.ADMIN] },
  { label: 'Clinic', path: '/clinic', roles: [Role.ADMIN] },
  {
    label: 'Patients',
    path: '/patients',
    roles: [Role.NURSE, Role.PRACTITIONER],
  },
  {
    label: 'Schedule',
    path: '/schedule',
    roles: [Role.NURSE, Role.PRACTITIONER],
  },
  { label: 'Examinations', path: '/examinations', roles: [Role.PRACTITIONER] },
];

export const MainLayout: React.FC = () => {
  const { user, logout } = useAuthStore(({ user, logout }) => ({
    user,
    logout,
  }));
  // set portal element
  const portalStore = usePortalStore();
  useEffect(() => portalStore.setElement(), []);

  return (
    <>
      {/* Top Nav */}
      <div className="fixed top-0 right-0 left-0 h-16 bg-teal-700 flex justify-between z-40">
        <div className="flex items-center pl-4 text-teal-50 w-48">
          <CustomLink to={{ pathname: '/' }}>
            <div>DDD Clinic</div>
          </CustomLink>
        </div>
        <div className="flex-grow flex items-center justify-start">
          <div id="nav-portal" className="flex-grow"></div>
        </div>
        <div className="flex items-center pr-4">
          <p className="mr-4 pr-4 text-teal-50 border-r-2 border-teal-900">
            Welcome, {user?.name}
          </p>
          <button className="btn btn-dark btn-sm" onClick={logout}>
            Logout
          </button>
        </div>
      </div>
      {/* Sidebar */}
      <div className="w-48 top-16 bottom-0 fixed bg-gray-200 z-40">
        <nav className="flex flex-col">
          {navUrls.map((u) => (
            <AuthGuard key={u.path} roles={u.roles}>
              <CustomLink
                to={{ pathname: u.path }}
                className="p-4 hover:bg-teal-900 hover:text-teal-50"
                activeClassName="font-bold bg-teal-700 text-teal-50"
              >
                {u.label}
              </CustomLink>
            </AuthGuard>
          ))}
        </nav>
      </div>
      {/* Main content */}
      <div className="ml-48 mt-16 z-10">
        <div className="p-4">
          <Outlet />
        </div>
      </div>
    </>
  );
};

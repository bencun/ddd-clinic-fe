import { Outlet } from 'react-router';

export const AuthLayout: React.FC = () => {
  return (
    <div className="h-screen v-screen flex justify-center items-center">
      <Outlet />
    </div>
  );
};

import React from 'react';
import { Navigate } from 'react-router';
import { useAuthStore } from '../../framework/store/AuthStore';
import { Role } from '../../framework/types/Role';

interface IAuthGuardProps {
  roles?: Role[];
  redirectToLogin?: boolean;
}
export const AuthGuard: React.FC<IAuthGuardProps> = ({
  children,
  roles,
  redirectToLogin = false,
}) => {
  const { loggedIn, user } = useAuthStore(({ loggedIn, user }) => ({
    loggedIn,
    user,
  }));

  let authorized = false;
  if (user && loggedIn) {
    if (roles && roles.length > 0) {
      if (roles && roles.includes(user.role)) {
        authorized = true;
      }
    } else {
      authorized = true;
    }
  }
  // rendering
  if (authorized) {
    return <>{children}</>;
  } else {
    if (redirectToLogin) {
      return <Navigate to={{ pathname: '/login' }} />;
    } else {
      return null;
    }
  }
};

import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { GetExaminationService } from '../../../framework/api/services/ExaminationServices';
import { Examination } from '../../../framework/models/Examination';
import { useToastStore } from '../../../framework/store/ToastStore';
import { ExceptionDetails } from '../../../framework/utils/ExceptionDetails';

export const ExaminationViewPage: React.FC = () => {
  const { uuid } = useParams<{ uuid: string }>();
  const [examination, setExamination] = useState<Examination>();
  const toast = useToastStore();

  const load = async () => {
    if (!uuid) return;

    try {
      const res = await GetExaminationService(uuid);
      if (res.success && res.data) {
        setExamination(res.data);
      } else throw new Error(res.error);
    } catch (e) {
      toast.show({
        text: ExceptionDetails('Failed to load the examination.', e),
        type: 'error',
      });
    }
  };

  useEffect(() => {
    load();
  }, [uuid]);

  if (!examination) return null;

  return (
    <div>
      <h1>Examination</h1>
      {uuid}
    </div>
  );
};

import classNames from 'classnames';
import { format } from 'date-fns';
import React, { useEffect, useState } from 'react';
import { useExamination } from '../../../framework/api/hooks/useExamination';
import { DomainEventToken } from '../../../framework/constants/DomainEventToken';
import {
  ExaminationStatus,
  ExaminationStatusLabel,
} from '../../../framework/constants/ExaminationStatus';
import { useSocket } from '../../../framework/hooks/useSocket';
import { Examination } from '../../../framework/models/Examination';
import { ListItem } from '../../atoms/ListItem';
import {
  ExaminationView,
  TExaminationForm,
} from '../../organisms/ExaminationView';

export const ExaminationsPage: React.FC = () => {
  const { examinations, loadActive, conclude } = useExamination();
  const [selectedExamination, setSelectedExamination] = useState<Examination>();

  useEffect(() => {
    loadActive();
  }, []);
  const onComplete = (d: TExaminationForm) => onSubmit(d, 'complete');
  const onCancel = (d: TExaminationForm) => onSubmit(d, 'cancel');

  const handleSocket = () => {
    loadActive();
  };
  useSocket(DomainEventToken.EXAMINATION_CANCELED, handleSocket);
  useSocket(DomainEventToken.EXAMINATION_COMPLETED, handleSocket);

  const onSubmit = (
    {
      anamnesis,
      diagnoses,
      prescriptions,
      notes,
      chronicDiagnosesEstablished,
      chronicDiagnosesRemoved,
    }: TExaminationForm,
    completionType: 'complete' | 'cancel'
  ) => {
    if (selectedExamination) {
      conclude(selectedExamination?.uuid, {
        anamnesis,
        notes: notes || '',
        diagnoses: diagnoses.map((d) => d.code),
        chronicDiagnosesEstablished: chronicDiagnosesEstablished.map(
          (d) => d.code
        ),
        chronicDiagnosesRemoved: chronicDiagnosesRemoved.map((d) => d.code),
        prescriptions: prescriptions,
        completionType,
      });
    }
  };

  // render
  if (!examinations || examinations.length === 0)
    return <div>No Active Examinations.</div>;
  // preselect
  if (!selectedExamination && examinations.length > 0) {
    setSelectedExamination(examinations[0]);
  }

  return (
    <div className="flex h-[calc(100vh-theme(space.24))] items-stretch">
      <div className="border-r-2 border-teal-700">
        {examinations.map((e) => (
          <ListItem
            className={classNames(selectedExamination === e && 'bg-gray-200')}
            key={e.uuid}
            header={
              <>
                <div className="font-bold">{e.patientName}</div>
                <div className="text-gray-500">
                  Started at:{format(e.startedAt, 'dd.MM.yyyy.')}
                </div>
              </>
            }
            footer={
              <>
                <button
                  className={classNames(
                    selectedExamination === e && 'hidden',
                    'btn btn-sm mr-4'
                  )}
                  onClick={() => setSelectedExamination(e)}
                >
                  Manage
                </button>
              </>
            }
          >
            <div className="flex flex-col text-sm">
              <div>
                Status: {ExaminationStatusLabel[e.status as ExaminationStatus]}
              </div>
            </div>
          </ListItem>
        ))}
      </div>

      <div className="flex-grow h-full overflow-y-auto">
        {selectedExamination && (
          <ExaminationView
            key={selectedExamination.uuid}
            examination={selectedExamination}
            onSubmit={onComplete}
            onCancel={onCancel}
          />
        )}
      </div>
    </div>
  );
};

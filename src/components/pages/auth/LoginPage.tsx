import { useForm } from 'react-hook-form';
import { Navigate } from 'react-router';
import { useAuthStore } from '../../../framework/store/AuthStore';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useLoading } from '../../../framework/hooks/useLoading';
import { InputWrapper } from '../../atoms/InputWrapper';

type LoginForm = {
  email: string;
  password: string;
};

const LoginSchema = yup.object().shape({
  email: yup.string().email(),
  password: yup.string().min(8),
});

export const LoginPage: React.FC = () => {
  const { loggedIn, login } = useAuthStore(({ loggedIn, login }) => ({
    loggedIn,
    login,
  }));

  const { isLoading, startLoading, stopLoading } = useLoading();

  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<LoginForm>({ resolver: yupResolver(LoginSchema) });

  const onSubmit = async (data: LoginForm) => {
    startLoading();
    await login(data);
    stopLoading();
  };

  if (loggedIn) {
    return <Navigate to={{ pathname: '/' }} />;
  }
  return (
    <div className="card">
      <h1 className="text-center">Login</h1>
      <form onSubmit={handleSubmit(onSubmit)} className="flex flex-col">
        <InputWrapper label="Email" error={errors.email?.message}>
          <input type="text" {...register('email')} />
        </InputWrapper>
        <InputWrapper label="Password" error={errors.password?.message}>
          <input type="password" {...register('password')} />
        </InputWrapper>

        <button className="btn" type="submit" disabled={isLoading}>
          Login
        </button>
      </form>
    </div>
  );
};

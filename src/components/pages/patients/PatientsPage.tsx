import { useNavigate } from 'react-router';
import {
  PaginationHookLoader,
  usePagination,
} from '../../../framework/api/hooks/usePagination';
import { PatientAPIService } from '../../../framework/api/services/PatientServices';
import { Patient } from '../../../framework/models/Patient';
import { usePortal } from '../../../framework/store/PortalStore';
import { Role } from '../../../framework/types/Role';
import { ListItem } from '../../atoms/ListItem';
import { PaginationController } from '../../molecules/PaginationController';
import { AuthGuard } from '../../utils/AuthGuard';

const loader: PaginationHookLoader<Patient> = async (p) => {
  return await PatientAPIService.getAll(p);
};

export const PatientsPage: React.FC = () => {
  const paginationHook = usePagination(loader);
  const { list } = paginationHook;
  const navigate = useNavigate();

  const navPortal = usePortal(
    <div className="flex items-center">
      <PaginationController hook={paginationHook} />
      <AuthGuard roles={[Role.NURSE]}>
        <button
          className="btn btn-sm btn-outline ml-2"
          onClick={() => navigate({ pathname: 'create' })}
        >
          Create New Patient
        </button>
      </AuthGuard>
    </div>
  );

  return (
    <div>
      {navPortal}
      {list.map((p) => (
        <ListItem
          key={p.uuid}
          header={
            <>
              <div className="font-bold">{p.name}</div>
              <div className="text-sm">
                LBO: {p.lbo}, JMBG: {p.jmbg}
              </div>
            </>
          }
          footer={
            <div>
              <button
                className="btn btn-sm mr-4"
                onClick={() => navigate({ pathname: p.uuid })}
              >
                Profile
              </button>
              <button
                className="btn btn-sm btn-outline"
                onClick={() => navigate({ pathname: `${p.uuid}/records` })}
              >
                Health Records
              </button>
            </div>
          }
        >
          <div className="flex flex-col text-sm">
            <div>City: {p.city}</div>
            <div>Email: {p.email}</div>
            <div>Phone: {p.phone}</div>
          </div>
        </ListItem>
      ))}
    </div>
  );
};

import { useParams } from 'react-router';
import { boolean } from 'yup/lib/locale';
import { usePatient } from '../../../framework/api/hooks/usePatient';
import { DomainEventToken } from '../../../framework/constants/DomainEventToken';
import { useSocket } from '../../../framework/hooks/useSocket';
import {
  PatientProfileForm,
  TPatientProfileForm,
} from '../../molecules/PatientProfileForm';
import { HealthRecords } from '../../organisms/HealthRecords';

interface IViewPatientPageProps {
  showHealthRecords?: boolean;
}
export const ViewPatientPage: React.FC<IViewPatientPageProps> = ({
  showHealthRecords,
}) => {
  const params = useParams<{ uuid: string }>();
  const { patient, update, isLoading, load } = usePatient(
    params.uuid as string
  );

  useSocket(DomainEventToken.PATIENT_PROFILE_UPDATED, (d) => {
    if (params.uuid && d) {
      params.uuid === (d as { patientUuid: string })?.patientUuid &&
        load(params.uuid);
    }
  });

  const onSubmit = (p: TPatientProfileForm) => {
    update(params.uuid as string, p);
  };

  if (!patient || isLoading) return null;

  return (
    <>
      <h1 className="mb-4 px-2">Patient Profile</h1>
      <PatientProfileForm
        patient={patient}
        onSubmit={onSubmit}
        disabled={isLoading}
      />
      <HealthRecords
        healthRecords={patient.healthRecords}
        show={showHealthRecords}
      />
    </>
  );
};

import React from 'react';
import { usePatient } from '../../../framework/api/hooks/usePatient';
import { PatientProfileForm } from '../../molecules/PatientProfileForm';

export const CreatePatientPage: React.FC = () => {
  const { create } = usePatient();

  return (
    <>
      <h1 className="mb-4 px-2">Create a New Patient</h1>
      <PatientProfileForm onSubmit={(p) => create(p)} />
    </>
  );
};

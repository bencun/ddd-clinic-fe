import React, { useEffect, useMemo, useState } from 'react';
import { useNavigate, useParams } from 'react-router';
import { addWeeks, subWeeks, isValid, parse } from 'date-fns';
import { usePortal } from '../../../framework/store/PortalStore';
import { useSchedule } from '../../../framework/api/hooks/useSchedule';
import { startOfWeek } from 'date-fns';
import { ScheduleCalendar } from '../../organisms/ScheduleCalendar';
import format from 'date-fns/format';
import { useAuthStore } from '../../../framework/store/AuthStore';
import { useSocket } from '../../../framework/hooks/useSocket';
import { DomainEventToken } from '../../../framework/constants/DomainEventToken';

export const SchedulePage: React.FC = () => {
  const params = useParams<{ date: string }>();
  const dateParam = params.date as string;
  const [date, setDate] = useState<Date>();
  // use schedule
  const { schedule, load, isLoading } = useSchedule(date);
  const navigate = useNavigate();
  const clinic = useAuthStore((s) => s.clinic);
  const roomsAvailable = useMemo(
    () => (clinic?.preferences.get('CLINIC_NUMBER_OF_ROOMS') as number) || 0,
    [clinic]
  );

  // parse param date
  useEffect(() => {
    let realDate = new Date();
    if (dateParam !== 'today') {
      realDate = parse(dateParam, 'yyyy-MM-dd', new Date());
      if (!realDate || !isValid(realDate)) {
        // error state, load for current week
        realDate = new Date();
      }
    }
    setDate(startOfWeek(realDate, { weekStartsOn: 1 }));
  }, [params]);

  // load whenever date changes
  useEffect(() => {
    date && load(date);
  }, [date]);

  const reload = () => {
    date && load(date);
  };

  useSocket(DomainEventToken.SCHEDULE_UPDATED, reload);

  const prevWeek = () => {
    !isLoading &&
      date &&
      navigate({
        pathname: `../schedule/${format(subWeeks(date, 1), 'yyyy-MM-dd')}`,
      });
  };

  const nextWeek = () => {
    !isLoading &&
      date &&
      navigate({
        pathname: `../schedule/${format(addWeeks(date, 1), 'yyyy-MM-dd')}`,
      });
  };

  // portal for actions
  const actions = usePortal(
    <div className="flex items-center">
      <button
        className="btn btn-outline btn-xs px-3 mr-2"
        onClick={prevWeek}
        disabled={isLoading}
      >
        &lt;
      </button>
      <button
        className="btn btn-outline btn-xs px-3 mr-2"
        onClick={nextWeek}
        disabled={isLoading}
      >
        &gt;
      </button>
      <div className="text-teal-100 text-sm">
        {date && `Week ${format(date, 'w.')}`}
      </div>
    </div>
  );

  if (!date) return null;
  if (!schedule) return null;

  return (
    <>
      {actions}
      <ScheduleCalendar schedule={schedule} roomsAvailable={roomsAvailable} />
    </>
  );
};

import React, { useEffect } from 'react';
import { useClinic } from '../../../framework/api/hooks/useClinic';
import { ClinicForm } from '../../molecules/ClinicForm';

export const ClinicPage: React.FC = () => {
  const { clinic, load, save } = useClinic();
  useEffect(() => {
    load();
  }, []);

  return <div>{clinic && <ClinicForm clinic={clinic} onSubmit={save} />}</div>;
};

import React from 'react';
import { useNavigate } from 'react-router';
import { useUsers } from '../../../framework/api/hooks/useUsers';
import { usePortal } from '../../../framework/store/PortalStore';
import { RoleLabel } from '../../../framework/types/Role';
import { ListItem } from '../../atoms/ListItem';

export const UsersPage: React.FC = () => {
  const users = useUsers();
  const navigate = useNavigate();

  const navPortal = usePortal(
    <button
      className="btn btn-sm btn-outline"
      onClick={() => navigate({ pathname: 'create' })}
    >
      Create a new User
    </button>
  );

  if (!users) {
    return null;
  }

  return (
    <div>
      {navPortal}
      {users.map((u) => (
        <ListItem
          key={u.uuid}
          header={
            <>
              <div className="font-bold">{u.name}</div>
              <div className="text-gray-500">Role: {RoleLabel[u.role]}</div>
            </>
          }
          footer={
            <>
              <button
                className="btn btn-sm mr-4"
                onClick={() => navigate({ pathname: u.uuid })}
              >
                View Profile
              </button>
            </>
          }
        >
          <div className="flex flex-col text-sm">
            <div>Email: {u.email}</div>
            <div>Phone: {u.phone}</div>
          </div>
        </ListItem>
      ))}
    </div>
  );
};

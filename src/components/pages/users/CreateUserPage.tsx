import React from 'react';
import { useUser } from '../../../framework/api/hooks/useUser';
import { UserProfileForm } from '../../molecules/UserProfileForm';

export const CreateUserPage: React.FC = () => {
  const { create } = useUser();

  return (
    <>
      <h1 className="mb-4 px-2">Create a New User</h1>
      <UserProfileForm
        onSubmit={(data) => {
          create(data);
        }}
      />
    </>
  );
};

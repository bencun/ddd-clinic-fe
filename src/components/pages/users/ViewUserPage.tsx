import React from 'react';
import { useParams } from 'react-router';
import { useUser } from '../../../framework/api/hooks/useUser';
import { useConfirmationModal } from '../../../framework/hooks/useConfirmationModal';
import { UserProfileForm } from '../../molecules/UserProfileForm';

export const ViewUserPage: React.FC = () => {
  const params = useParams<{ uuid: string }>();
  const { user, update, requestPasswordReset, deleteUser } = useUser(
    params.uuid as string
  );
  const { confirmationModal, show } = useConfirmationModal();

  const showDeleteConfirmation = (uuid: string) => show(() => deleteUser(uuid));

  if (!user) return null;
  return (
    <div>
      {confirmationModal}
      <h1 className="mb-4 px-2">User Profile</h1>
      <UserProfileForm
        user={user}
        onSubmit={(f) => update(params.uuid as string, f)}
      />

      <div className="mb-4 px-2">
        <h2 className="mb-4">Administrative Actions</h2>
        <button
          className="btn mr-4"
          onClick={() => requestPasswordReset({ email: user.email })}
        >
          Reset Password
        </button>

        <button
          className="btn btn-warning"
          onClick={() => showDeleteConfirmation(user.uuid)}
        >
          Delete User
        </button>
      </div>
    </div>
  );
};
